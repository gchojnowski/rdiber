//Copyright (c) 2002-2007 John D. Hunter; All Rights Reserved

#include <stdlib.h>
#include <stdio.h>


typedef short Cdata;
typedef struct Csite Csite;
struct Csite
{
    long edge;                  /* ij of current edge */
    long left;                  /* +-1 or +-imax as the zone is to right, left, below,
                                 * or above the edge */
    long imax;                  /* imax for the mesh */
    long jmax;                  /* jmax for the mesh */
    long n;                     /* number of points marked on this curve so far */
    long count;                 /* count of start markers visited */
    double zlevel[2];           /* contour levels, zlevel[1]<=zlevel[0]
                                 * signals single level case */
    short *triangle;            /* triangulation array for the mesh */
    char *reg;                   /* region array for the mesh (was int) */
    Cdata *data;                /* added by EF */
    long edge0, left0;          /* starting site on this curve for closure */
    int level0;                 /* starting level for closure */
    long edge00;                /* site needing START_ROW mark */

    /* making the actual marks requires a bunch of other stuff */
    const double *x, *y, *z;    /* mesh coordinates and function values */
    double *xcp, *ycp;          /* output contour points */
};

void print_Csite(Csite *Csite);

/* triangle only takes values of -1, 0, 1, so it could be a signed char. */
/* most or all of the longs probably could be converted to ints with no loss */

/* the Cdata array consists of the following bits:
 * Z_VALUE     (2 bits) 0, 1, or 2 function value at point
 * ZONE_EX     1 zone exists, 0 zone doesn't exist
 * I_BNDY      this i-edge (i=constant edge) is a mesh boundary
 * J_BNDY      this j-edge (i=constant edge) is a mesh boundary
 * I0_START    this i-edge is a start point into zone to left
 * I1_START    this i-edge is a start point into zone to right
 * J0_START    this j-edge is a start point into zone below
 * J1_START    this j-edge is a start point into zone above
 * START_ROW   next start point is in current row (accelerates 2nd pass)
 * SLIT_UP     marks this i-edge as the beginning of a slit upstroke
 * SLIT_DN     marks this i-edge as the beginning of a slit downstroke
 * OPEN_END    marks an i-edge start point whose other endpoint is
 *             on a boundary for the single level case
 * ALL_DONE    marks final start point
 */

#define Z_VALUE   0x0003
#define ZONE_EX   0x0004
#define I_BNDY    0x0008
#define J_BNDY    0x0010
#define I0_START  0x0020
#define I1_START  0x0040
#define J0_START  0x0080
#define J1_START  0x0100
#define START_ROW 0x0200
#define SLIT_UP   0x0400
#define SLIT_DN   0x0800
#define OPEN_END  0x1000
#define ALL_DONE  0x2000



/* some helpful macros to find points relative to a given directed
 * edge -- points are designated 0, 1, 2, 3 CCW around zone with 0 and
 * 1 the endpoints of the current edge */
#define FORWARD(left,ix) ((left)>0?((left)>1?1:-(ix)):((left)<-1?-1:(ix)))
#define POINT0(edge,fwd) ((edge)-((fwd)>0?fwd:0))
#define POINT1(edge,fwd) ((edge)+((fwd)<0?fwd:0))
#define IS_JEDGE(edge,left) ((left)>0?((left)>1?1:0):((left)<-1?1:0))
#define ANY_START (I0_START|I1_START|J0_START|J1_START)
#define START_MARK(left) \
  ((left)>0?((left)>1?J1_START:I1_START):((left)<-1?J0_START:I0_START))

/* ------------------------------------------------------------------------ */

/* these actually mark points */
//static int zone_crosser (Csite * site, int level, int pass2);
//static int edge_walker (Csite * site, int pass2);
//static int slit_cutter (Csite * site, int up, int pass2);

/* this calls the first three to trace the next disjoint curve
 * -- return value is number of points on this curve, or
 *    0 if there are no more curves this pass
 *    -(number of points) on first pass if:
 *      this is two level case, and the curve closed on a hole
 *      this is single level case, curve is open, and will start from
 *      a different point on the second pass
 *      -- in both cases, this curve will be combined with another
 *         on the second pass */
//static long curve_tracer (Csite * site, int pass2);

/* this initializes the data array for curve_tracer */
//static void data_init (Csite * site, int region, long nchunk);

/* ------------------------------------------------------------------------ */

/* zone_crosser assumes you are sitting at a cut edge about to cross
 * the current zone.  It always marks the initial point, crosses at
 * least one zone, and marks the final point.  On non-boundary i-edges,
 * it is responsible for removing start markers on the first pass.  */
int zone_crosser (Csite * site, int level, int pass2);


/* edge_walker assumes that the current edge is being drawn CCW
 * around the current zone.  Since only boundary edges are drawn
 * and we always walk around with the filled region to the left,
 * no edge is ever drawn CW.  We attempt to advance to the next
 * edge on this boundary, but if current second endpoint is not
 * between the two contour levels, we exit back to zone_crosser.
 * Note that we may wind up marking no points.
 * -- edge_walker is never called for single level case */
int edge_walker (Csite * site,  int pass2);

/* -- slit_cutter is never called for single level case */
int slit_cutter (Csite * site, int up, int pass2);
/* ------------------------------------------------------------------------ */

/* curve_tracer finds the next starting point, then traces the curve,
 * returning the number of points on this curve
 * -- in a two level trace, the return value is negative on the
 *    first pass if the curve closed on a hole
 * -- in a single level trace, the return value is negative on the
 *    first pass if the curve is an incomplete open curve
 * -- a return value of 0 indicates no more curves */
long curve_tracer (Csite * site, int pass2);
/* ------------------------------------------------------------------------ */

/* The sole function of the "region" argument is to specify the
   value in Csite.reg that denotes a missing zone.  We always
   use zero.
*/

int data_init (Csite * site, int region, long nchunk);
