#include "rdiber_aniso.h"


//******************************************************************************
//
//  Clipper anisotropy correction
//
//  sfscale.cpp: structure factor anisotropic scaling implementation
//  Copyright (C) 2000-2006 Kevin Cowtan and University of York
//
//  Code restricting resolution for calculating the anisotropic scaling factors
//  added by Grzegorz Chojnowski 2009
//
//******************************************************************************


int clipper_anisotropy_correction( clipper::HKL_data< clipper::datatypes::F_sigF<float> >& fs_input, float invresolsq_min, float invresolsq_max ){


  float nsig = 0.0;
  float invresolsq;
  std::vector<float> aniso_eigenvector;

  typedef clipper::HKL_info::HKL_reference_index HRI;
  // expand to P1 in order to preserve symmetry
  const clipper::HKL_info& hkls = fs_input.hkl_info();
  clipper::Spacegroup spgrp1( clipper::Spacegroup::P1 );
  clipper::HKL_info hkl1( spgrp1, hkls.cell(), hkls.resolution(), true );
  clipper::HKL_data<clipper::datatypes::F_sigF<float> > fo1( hkl1 ), fs1( hkl1 ), fc1( hkl1 );


  for ( clipper::HKL_info::HKL_reference_index ih = hkl1.first(); !ih.last(); ih.next() ) {
    clipper::datatypes::F_sigF<float> f = fs_input[ih.hkl()];
    if ( (f.f() >= nsig * f.sigf()) && !contains(invresolsq_min, (invresolsq=ih.invresolsq()), invresolsq_max) ) fs1[ih] = fo1[ih] = f;
  }


  // perform aniso scaling 3 times to allow aniso scale from previous
  // cycle to correct for missing data on the next cycle:
  // start with unscaled data and iterate
  clipper::BasisFn_log_aniso_gaussian bfn;
  std::vector<double> param( 7, 0.0 ), params( 12, 1.0 );
  for ( int cycno = 0; cycno < 3; cycno++ ) {
    // create artificial F's from mean F with resolution
    clipper::TargetFn_meanFnth<clipper::datatypes::F_sigF<float> > tfns( fs1, 2.0 );
    clipper::BasisFn_spline bfns( fs1, 12 );
    clipper::ResolutionFn rfns( hkl1, bfns, tfns, params );
  

    for ( HRI ih = hkl1.first(); !ih.last(); ih.next() )
      fc1[ih] = clipper::datatypes::F_sigF<float>( sqrt(rfns.f(ih)), 1.0 );

    // do the aniso scaling
    clipper::TargetFn_scaleLogF1F2< clipper::datatypes::F_sigF<float>, clipper::datatypes::F_sigF<float> > tfn( fo1, fc1 );
    
    clipper::ResolutionFn rfn( hkl1, bfn, tfn, param );
 
    param = rfn.params();

    
    // set trace to zero (i.e. no isotropic correction)
    float dp = (param[1]+param[2]+param[3])/3.0;
    param[1] -= dp;
    param[2] -= dp;
    param[3] -= dp;


    // create new scaled list correct with current estimate
    fs1 = fo1;
    for ( HRI ih = hkl1.first(); !ih.last(); ih.next() )
      if ( !fs1[ih].missing() )
        fs1[ih].scale( exp( 0.5*bfn.f(ih.hkl(),hkl1.cell(), param) ) );
  }

  clipper::Matrix<float> m(3,3);
  m(0,0)=       param[1]; m(1,1)=       param[2]; m(2,2)=       param[3];
  m(0,1)=m(1,0)=param[4]; m(0,2)=m(2,0)=param[5]; m(1,2)=m(2,1)=param[6];
  aniso_eigenvector = m.eigen();

  
  double dp = 0.0;
  dp = aniso_eigenvector[0];
  param[1] -= dp; param[2] -= dp; param[3] -= dp;
  


  // store the results
  for ( HRI ih = hkls.first(); !ih.last(); ih.next() )
    if ( !fs_input[ih].missing() )
      fs_input[ih].scale( exp( 0.5*bfn.f(ih.hkl(),hkls.cell(),param) ) );

  return 1;
}
