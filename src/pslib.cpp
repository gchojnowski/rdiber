//******************************************************************************
//
//                PostScriptLib 1.0.1 (07.05.2007)
//                    based on PostScript fortran subroutines PS.F
//                    written by Roman Laskowski (PROCHECK)
//                    and modified by Alexei Vagin (MOLREP)
//                                   
//******************************************************************************

#include "pslib.h"

#define PS_SCALE 1.0




// -----------------------------------------------------------------------------
//
//  assign rgb colors table
//
// -----------------------------------------------------------------------------

float * set_rgb(int * maxcolor){
	*maxcolor = 19;
	float * rgb = (float*)(calloc(3*(*maxcolor), sizeof(float)));
	int icol=0;
	
	//0 - black
	rgb[3*icol+0]=0.0;
	rgb[3*icol+1]=0.0;
	rgb[3*icol+2]=0.0;
	icol++;
	
	//1 - white
	rgb[3*icol+0]=1.0;
	rgb[3*icol+1]=1.0;
	rgb[3*icol+2]=1.0;
	icol++;
	
	//2 - red
	rgb[3*icol+0]=1.0;
	rgb[3*icol+1]=0.0;
	rgb[3*icol+2]=0.0;
	icol++;
	
	//3 - green
	rgb[3*icol+0]=0.0;
	rgb[3*icol+1]=1.0;
	rgb[3*icol+2]=0.0;
	icol++;
	
	//4 - blue
	rgb[3*icol+0]=0.0;
	rgb[3*icol+1]=0.0;
	rgb[3*icol+2]=1.0;
	icol++;
	
	//5 - yellow
	rgb[3*icol+0]=1.0;
	rgb[3*icol+1]=1.0;
	rgb[3*icol+2]=0.0;
	icol++;
	
	//6 - orange
	rgb[3*icol+0]=0.8;
	rgb[3*icol+1]=0.5;
	rgb[3*icol+2]=0.0;
	icol++;
	
	//7 - lime green
	rgb[3*icol+0]=0.5;
	rgb[3*icol+1]=1.0;
	rgb[3*icol+2]=0.0;
	icol++;
	
	//8 - purple
	rgb[3*icol+0]=0.5;
	rgb[3*icol+1]=0.0;
	rgb[3*icol+2]=1.0;
	icol++;
	
	//9 - cyan
	rgb[3*icol+0]=0.5;
	rgb[3*icol+1]=1.0;
	rgb[3*icol+2]=1.0;
	icol++;
	
	//10 - pink
	rgb[3*icol+0]=1.0;
	rgb[3*icol+1]=0.5;
	rgb[3*icol+2]=1.0;
	icol++;
	
	//11 - sky blue
	rgb[3*icol+0]=0.0;
	rgb[3*icol+1]=0.5;
	rgb[3*icol+2]=1.0;
	icol++;
	
	//12 - cream
	rgb[3*icol+0]=1.0;
	rgb[3*icol+1]=1.0;
	rgb[3*icol+2]=0.7;
	icol++;
	
	//13 - turquoise
	rgb[3*icol+0]=0.0;
	rgb[3*icol+1]=1.0;
	rgb[3*icol+2]=1.0;
	icol++;
	
	//14 - lilac
	rgb[3*icol+0]=1.0;
	rgb[3*icol+1]=0.0;
	rgb[3*icol+2]=1.0;
	icol++;
	
	//15 - brick red
	rgb[3*icol+0]=0.8;
	rgb[3*icol+1]=0.0;
	rgb[3*icol+2]=0.0;
	icol++;
	
	//16 - brown
	rgb[3*icol+0]=0.5;
	rgb[3*icol+1]=0.0;
	rgb[3*icol+2]=0.0;
	icol++;
	
	//17 - light grey
	rgb[3*icol+0]=0.97;
	rgb[3*icol+1]=0.97;
	rgb[3*icol+2]=0.97;
	icol++;
	
	//18 - white
	rgb[3*icol+0]=1.0;
	rgb[3*icol+1]=1.0;
	rgb[3*icol+2]=1.0;
	
	return rgb;
}






// -----------------------------------------------------------------------------
//
//   psopen -  Create new PostScript file
//
// -----------------------------------------------------------------------------

FILE * psopen(char * fname, int bboxx1, int bboxx2, int bboxy1, int bboxy2, int * maxcolor,\
	      float * rgb, int incol, int bakcol, int ierr, int total_pages, int  current_page){
	//rgb - 3 x maxcolor matrix
  
  
  
  rgb = set_rgb(maxcolor);
  
  
  FILE * psfile = fopen(fname, "w");
  fprintf(psfile, "%%!PS-Adobe-3.0 EPSF-3.0\n");
  fprintf(psfile, "%sCreator: DIBER PostScriptLib\n", "%%");
  fprintf(psfile, "%sDocumentNeededResources: font Times-Roman Symbol Times-Bold\n", "%%");
  fprintf(psfile, "%sBoundingBox: (atend)\n", "%%");
  fprintf(psfile, "%sPages:%3i\n", "%%", total_pages);
  fprintf(psfile, "%sEndComments\n", "%%");
  fprintf(psfile, "%sBeginProlog\n", "%%");
  
  psresource_new(psfile);
	
	
  fprintf(psfile, "/L { moveto lineto stroke } bind def\n/Col { setrgbcolor } bind def\n");
  if(incol==1)
    for(int icol=0; icol<(*maxcolor); icol++)
      fprintf(psfile, "/Col%02i {gsave %8.4f%8.4f%8.4f setrgbcolor } def\n", icol, rgb[3*icol], rgb[3*icol+1], rgb[3*icol+2]);
  //new 27-05-2009
 fprintf(psfile, " /m { moveto } bind def\n");
 fprintf(psfile, "/l { lineto } bind def\n");
 fprintf(psfile, "/clipbox {\n");
 fprintf(psfile, "box\n");
 fprintf(psfile, "clip\n");
 fprintf(psfile, "newpath\n");
 fprintf(psfile, "} bind def\n");
 


  
  fprintf(psfile, "/Poly3 { moveto lineto lineto fill grestore } bind def\n");
  fprintf(psfile, "/Pl3 { 6 copy Poly3 moveto moveto moveto closepath stroke } bind def\n");
  fprintf(psfile, "/Pline3 { 6 copy Poly3 moveto lineto lineto closepath stroke } bind def\n");
  
  
  fprintf(psfile, "/Poly4 { moveto lineto lineto lineto fill grestore } bind def\n");
  fprintf(psfile, "/Pl4 { 8 copy Poly4 moveto moveto moveto moveto closepath stroke } bind def\n");
  fprintf(psfile, "/Pline4 { 8 copy Poly4 moveto lineto lineto lineto closepath stroke } bind def\n");
  fprintf(psfile, "/D {gsave 1 setlinewidth 2 setlinecap 0 setlinejoin newpath 3 copy 0 360 arc fill grestore} bind def\n");
	
  fprintf(psfile, "/Circle { gsave newpath 3 copy 0 360 arc stroke grestore } bind def\n");
  
  
  fprintf(psfile, "/MB {gsave translate MFAC dup scale 1 setlinewidth 2 setlinecap 0 setlinejoin newpath} bind def\n");
  fprintf(psfile, "/FC {0 360 arc fill} bind def\n");
  fprintf(psfile, "/ME /grestore load def\n");
  //fprintf(psfile, "/PT {MB 0 0 1 FC ME} bind def\n");
  fprintf(psfile, "/MFAC      5.00 def\n");
  
  
  
  fprintf(psfile, "/Print { /Times-Roman-Extd findfont exch scalefont setfont show } bind def\n");
  fprintf(psfile, "/Bprint { /Times-Bold findfont exch scalefont setfont show } bind def\n");
  fprintf(psfile, "/Gprint { /Symbol findfont exch scalefont setfont show } bind def\n");
  fprintf(psfile, "/Center {\n");
  fprintf(psfile, "  dup /Times-Roman-Extd findfont exch scalefont setfont\n");
  fprintf(psfile, "  exch stringwidth pop -2 div exch -3 div rmoveto\n");
  fprintf(psfile, " } bind def\n");
  fprintf(psfile, "/CenterRot90 {\n");
  fprintf(psfile, "  dup /Times-Roman-Extd findfont exch scalefont setfont\n");
  fprintf(psfile, "  exch stringwidth pop -2 div exch 3 div exch rmoveto\n");
  fprintf(psfile, " } bind def\n");
  fprintf(psfile, "/UncenterRot90 {\n");
  fprintf(psfile, "  dup /Times-Roman-Extd findfont exch scalefont setfont\n");
  fprintf(psfile, "  exch stringwidth } bind def\n");
  fprintf(psfile, "/Rot90 { gsave currentpoint translate 90 rotate } bind def\n");
  fprintf(psfile, "%sEndProlog\n", "%%");

  fprintf(psfile, "%sBeginPageSetup\n", "%%");
  fprintf(psfile, "%10f%10f scale\n", PS_SCALE, PS_SCALE);
  fprintf(psfile, "%sEndPageSetup\n", "%%");

	
  fprintf(psfile, "%sBeginSetup\n", "%%");
  fprintf(psfile, "1 setlinecap 1 setlinejoin 1 setlinewidth 0 setgray [ ] 0 setdash newpath\n");
  fprintf(psfile, "/Times-Roman /Times-Roman-Extd encode_vector ReEncodeSmall\n");
  fprintf(psfile, "%sEndSetup\n", "%%");
  fprintf(psfile, "%sPage:%5i%5i\n", "%%", current_page, total_pages);
  
  fprintf(psfile, "/PSLibSave save def\n");
  fprintf(psfile, "%7.0f%7.0f moveto%7.0f%7.0f lineto%7.0f%7.0f lineto\n", float(bboxx1)/PS_SCALE, \
	  float(bboxy1)/PS_SCALE, float(bboxx2)/PS_SCALE, float(bboxy1)/PS_SCALE, float(bboxx2)/PS_SCALE,\
	  float(bboxy2)/PS_SCALE );
  fprintf(psfile, "%7.0f%7.0f lineto closepath gsave\n", float(bboxx1)/PS_SCALE, float(bboxy2)/PS_SCALE );
  fprintf(psfile, "gsave 1.0000 setgray fill grestore\n");
  fprintf(psfile, "clip newpath\n");
  
  if(incol){
    psshade(psfile, 0.0, bakcol, rgb,  *maxcolor, incol);
    psbbox(psfile, float(bboxx1), float(bboxy1), float(bboxx2), float(bboxy1), float(bboxx2), float(bboxy2),\
	   float(bboxx1), float(bboxy2));
  }
  
  
  
  return psfile;
}






// -----------------------------------------------------------------------------
//
//   psresource_new -  Write resource to PostScript file
//
// -----------------------------------------------------------------------------

int psresource_new(FILE * psfile){
  fprintf(psfile, "%sBeginResource: latin1_prolog\n", "%%");
  fprintf(psfile, " \n");
  fprintf(psfile, "%% ReEncodeSmall from Adobe Postscript Tutorial and Cookbook\n");
  fprintf(psfile, "/reencsmalldict 12 dict def\n");
  fprintf(psfile, "/ReEncodeSmall\n");
  fprintf(psfile, " {reencsmalldict begin\n");
  fprintf(psfile, "  /newcodesandnames exch def\n");
  fprintf(psfile, "  /newfontname exch def\n");
  fprintf(psfile, "  /basefontname exch def\n");
  fprintf(psfile, " \n");
  fprintf(psfile, "  /basefontdict basefontname findfont def\n");
  fprintf(psfile, "  /newfont basefontdict maxlength dict def\n");
  fprintf(psfile, " \n");
  fprintf(psfile, "  basefontdict\n");
  fprintf(psfile, "   {exch dup /FID ne\n");
  fprintf(psfile, "     {dup /Encoding eq\n");
  fprintf(psfile, "       {exch dup length array copy\n");
  fprintf(psfile, "  newfont 3 1 roll put}\n");
  fprintf(psfile, "       {exch newfont 3 1 roll put}\n");
  fprintf(psfile, "       ifelse\n");
  fprintf(psfile, "     }\n");
  fprintf(psfile, "     {pop pop}\n");
  fprintf(psfile, "     ifelse\n");
  fprintf(psfile, "   }forall\n");
  fprintf(psfile, " \n");
  fprintf(psfile, "  newfont /FontName newfontname put\n");
  fprintf(psfile, "  newcodesandnames aload pop\n");
  fprintf(psfile, " \n");
  fprintf(psfile, "  newcodesandnames length 2 idiv\n");
  fprintf(psfile, "   {newfont /Encoding get 3 1 roll put}\n");
  fprintf(psfile, "   repeat\n");
  fprintf(psfile, " \n");
  fprintf(psfile, "  newfontname newfont definefont pop\n");
  fprintf(psfile, "  end\n");
  fprintf(psfile, "}def\n");
  fprintf(psfile, "%sEndResource\n", "%%");
  fprintf(psfile, "%sBeginResource: latin1_encoding\n", "%%");
  fprintf(psfile, "/encode_vector [\n");
  fprintf(psfile, "8#040  /space        8#041  /exclam       8#042  /quotedbl     \n");
  fprintf(psfile, "8#043  /numbersign   8#044  /dollar       8#045  /percent      \n");
  fprintf(psfile, "8#046  /ampersand    8#047  /quoteright   8#050  /parenleft    \n");
  fprintf(psfile, "8#051  /parenright   8#052  /asterisk     8#053  /plus         \n");
  fprintf(psfile, "8#054  /comma        8#055  /minus        8#056  /period       \n");
  fprintf(psfile, "8#057  /slash        8#060  /zero         8#061  /one          \n");
  fprintf(psfile, "8#062  /two          8#063  /three        8#064  /four         \n");
  fprintf(psfile, "8#065  /five         8#066  /six          8#067  /seven        \n");
  fprintf(psfile, "8#070  /eight        8#071  /nine         8#072  /colon        \n");
  fprintf(psfile, "8#073  /semicolon    8#074  /less         8#075  /equal        \n");
  fprintf(psfile, "8#076  /greater      8#077  /question     8#100  /at           \n");
  fprintf(psfile, "8#101  /A            8#102  /B            8#103  /C            \n");
  fprintf(psfile, "8#104  /D            8#105  /E            8#106  /F            \n");
  fprintf(psfile, "8#107  /G            8#110  /H            8#111  /I            \n");
  fprintf(psfile, "8#112  /J            8#113  /K            8#114  /L            \n");
  fprintf(psfile, "8#115  /M            8#116  /N            8#117  /O            \n");
  fprintf(psfile, "8#120  /P            8#121  /Q            8#122  /R            \n");
  fprintf(psfile, "8#123  /S            8#124  /T            8#125  /U            \n");
  fprintf(psfile, "8#126  /V            8#127  /W            8#130  /X            \n");
  fprintf(psfile, "8#131  /Y            8#132  /Z            8#133  /bracketleft  \n");
  fprintf(psfile, "8#134  /backslash    8#135  /bracketright 8#136  /asciicircum  \n");
  fprintf(psfile, "8#137  /underscore   8#140  /quoteleft    8#141  /a            \n");
  fprintf(psfile, "8#142  /b            8#143  /c            8#144  /d            \n");
  fprintf(psfile, "8#145  /e            8#146  /f            8#147  /g            \n");
  fprintf(psfile, "8#150  /h            8#151  /i            8#152  /j            \n");
  fprintf(psfile, "8#153  /k            8#154  /l            8#155  /m            \n");
  fprintf(psfile, "8#156  /n            8#157  /o            8#160  /p            \n");
  fprintf(psfile, "8#161  /q            8#162  /r            8#163  /s            \n");
  fprintf(psfile, "8#164  /t            8#165  /u            8#166  /v            \n");
  fprintf(psfile, "8#167  /w            8#170  /x            8#171  /y            \n");
  fprintf(psfile, "8#172  /z            8#173  /braceleft    8#174  /bar          \n");
  fprintf(psfile, "8#175  /braceright   8#176  /tilde        8#240  /space        \n");
  fprintf(psfile, "8#241  /exclamdown   8#242  /cent         8#243  /sterling     \n");
  fprintf(psfile, "8#244  /currency     8#245  /yen          8#246  /brokenbar    \n");
  fprintf(psfile, "8#247  /section      8#250  /dieresis     8#251  /copyright    \n");
  fprintf(psfile, "8#252  /ordfeminine  8#253  /guillemotleft 8#254  /logicalnot  \n");
  fprintf(psfile, "8#255  /hyphen       8#256  /registered   8#257  /macron       \n");
  fprintf(psfile, "8#260  /degree       8#261  /plusminus    8#262  /twosuperior  \n");
  fprintf(psfile, "8#263  /threesuperior 8#264  /acute       8#265  /mu           \n");
  fprintf(psfile, "8#266  /paragraph    8#267  /bullet       8#270  /cedilla      \n");
  fprintf(psfile, "8#271  /dotlessi    8#272  /ordmasculine 8#273  /guillemotright\n");
  fprintf(psfile, "8#274  /onequarter   8#275  /onehalf      8#276  /threequarters\n");
  fprintf(psfile, "8#277  /questiondown 8#300  /Agrave       8#301  /Aacute       \n");
  fprintf(psfile, "8#302  /Acircumflex  8#303  /Atilde       8#304  /Adieresis    \n");
  fprintf(psfile, "8#305  /Aring        8#306  /AE           8#307  /Ccedilla     \n");
  fprintf(psfile, "8#310  /Egrave       8#311  /Eacute       8#312  /Ecircumflex  \n");
  fprintf(psfile, "8#313  /Edieresis    8#314  /Igrave       8#315  /Iacute       \n");
  fprintf(psfile, "8#316  /Icircumflex  8#317  /Idieresis    8#320  /Eth          \n");
  fprintf(psfile, "8#321  /Ntilde       8#322  /Ograve       8#323  /Oacute       \n");
  fprintf(psfile, "8#324  /Ocircumflex  8#325  /Otilde       8#326  /Odieresis    \n");
  fprintf(psfile, "8#327  /multiply     8#330  /Oslash       8#331  /Ugrave       \n");
  fprintf(psfile, "8#332  /Uacute       8#333  /Ucircumflex  8#334  /Udieresis    \n");
  fprintf(psfile, "8#335  /Yacute       8#336  /Thorn        8#337  /germandbls   \n");
  fprintf(psfile, "8#340  /agrave       8#341  /aacute       8#342  /acircumflex  \n");
  fprintf(psfile, "8#343  /atilde       8#344  /adieresis    8#345  /aring        \n");
  fprintf(psfile, "8#346  /ae           8#347  /ccedilla     8#350  /egrave       \n");
  fprintf(psfile, "8#351  /eacute       8#352  /ecircumflex  8#353  /edieresis    \n");
  fprintf(psfile, "8#354  /igrave       8#355  /iacute       8#356  /icircumflex  \n");
  fprintf(psfile, "8#357  /idieresis    8#360  /eth          8#361  /ntilde       \n");
  fprintf(psfile, "8#362  /ograve       8#363  /oacute       8#364  /ocircumflex  \n");
  fprintf(psfile, "8#365  /otilde       8#366  /odieresis    8#367  /divide       \n");
  fprintf(psfile, "8#370  /oslash       8#371  /ugrave       8#372  /uacute       \n");
  fprintf(psfile, "8#373  /ucircumflex  8#374  /udieresis    8#375  /yacute       \n");
  fprintf(psfile, "8#376  /thorn        8#377  /ydieresis           \n"); 			 
  fprintf(psfile, "] def\n"); 														 
  fprintf(psfile, "%sEndResource\n", "%%");






  //cube root only should be removed!!!!!!!!!!!!!!!!!!!!?
  fprintf(psfile, " %%!PS-Adobe-3.0 Resource-Font\n");
  fprintf(psfile, "   %%Title: cmex10\n");
  fprintf(psfile, "%%Copyright: Copyright (C) 1994, Basil K. Malyshev. All Rights Reserved.012BaKoMa Fonts Collection, Level-B.\n");
  fprintf(psfile, "%%Creator: Converted from TrueType by PPR\n");
  fprintf(psfile, "25 dict begin\n");
  fprintf(psfile, "/_d{bind def}bind def\n");
  fprintf(psfile, "/_m{moveto}_d\n");
  fprintf(psfile, "/_l{lineto}_d\n");
  fprintf(psfile, "/_cl{closepath eofill}_d\n");
  fprintf(psfile, "/_c{curveto}_d\n");
  fprintf(psfile, "/_sc{7 -1 roll{setcachedevice}{pop pop pop pop pop pop}ifelse}_d\n");
  fprintf(psfile, "/_e{exec}_d\n");
  fprintf(psfile, "/FontName /Cmex10 def\n");
  fprintf(psfile, "/PaintType 0 def\n");
  fprintf(psfile, "/FontMatrix[.001 0 0 .001 0 0]def\n");
  fprintf(psfile, "/FontBBox[-23 -2959 1454 772]def\n");
  fprintf(psfile, "/FontType 3 def\n");
  fprintf(psfile, "/Encoding StandardEncoding def\n");
  fprintf(psfile, "/FontInfo 10 dict dup begin\n");
  fprintf(psfile, "/FamilyName (cmex10) def\n");
  fprintf(psfile, "/FullName (cmex10) def\n");
  fprintf(psfile, "/Notice (Copyright (C) 1994, Basil K. Malyshev. All Rights Reserved.012BaKoMa Fonts Collection, Level-B. ) def\n");
  fprintf(psfile, "/Weight (Regular) def\n");
  fprintf(psfile, "/Version (1.1/12-Nov-94) def\n");
  fprintf(psfile, "/ItalicAngle 0.0 def\n");
  fprintf(psfile, "/isFixedPitch false def\n");
  fprintf(psfile, "/UnderlinePosition -133 def\n");
  fprintf(psfile, "/UnderlineThickness 20 def\n");
  fprintf(psfile, "end readonly def\n");
  fprintf(psfile, "/CharStrings 1 dict dup begin\n");
  fprintf(psfile, "/radicalbig{1000 0 110 -1159 1020 40 _sc\n");
  fprintf(psfile, "424 -1159 _m\n");
  fprintf(psfile, "198 -634 _l\n");
  fprintf(psfile, "128 -688 _l\n");
  fprintf(psfile, "110 -670 _l\n");
  fprintf(psfile, "253 -559 _l\n");
  fprintf(psfile, "464 -1050 _l\n");
  fprintf(psfile, "983 31 _l\n");
  fprintf(psfile, "986 37 992 40 1000 40 _c\n");
  fprintf(psfile, "1005 40 1010 38 1014 34 _c\n");
  fprintf(psfile, "1018 30 1020 25 1020 20 _c\n");
  fprintf(psfile, "1020 16 1019 14 1019 13 _c\n");
  fprintf(psfile, "461 -1150 _l\n");
  fprintf(psfile, "458 -1156 453 -1159 446 -1159 _c\n");
  fprintf(psfile, "424 -1159 _l\n");
  fprintf(psfile, "_cl}_d\n");
  fprintf(psfile, "end readonly def\n");
  
  fprintf(psfile, "/BuildGlyph\n");
  fprintf(psfile, " {exch begin\n");
  fprintf(psfile, " CharStrings exch\n");
  fprintf(psfile, " 2 copy known not{pop /.notdef}if\n");
  fprintf(psfile, " true 3 1 roll get exec\n");
  fprintf(psfile, " end}_d\n");
  
  fprintf(psfile, "/BuildChar {\n");
  fprintf(psfile, " 1 index /Encoding get exch get\n");
  fprintf(psfile, " 1 index /BuildGlyph get exec\n");
  fprintf(psfile, "}_d\n");
  
  fprintf(psfile, "FontName currentdict end definefont pop\n");
  fprintf(psfile, "%%EOF\n");



  
  return 1;
}

	
	
       
// -----------------------------------------------------------------------------
//			  
//    psnewpage  -  Write new page lines to a PostScript file				 
//			 
// -----------------------------------------------------------------------------

int psnewpage(FILE * psfile, int bboxx1, int bboxx2, int bboxy1, int bboxy2, int * maxcolor,\
	      float * rgb, int incol, int bakcol, int ierr, int total_pages, int  * current_page){
  
  (*current_page)+=1;
  
  
  fprintf(psfile, "grestore stroke\n");
  fprintf(psfile, "PSLibSave restore\n");
  fprintf(psfile, "showpage\n");
  
  fprintf(psfile, "%sPage:%3i%3i\n", "%%", *current_page, total_pages);
  
  fprintf(psfile, "/PSLibSave save def\n");
  fprintf(psfile, "%7.0f%7.0f moveto%7.0f%7.0f lineto%7.0f%7.0f lineto\n", float(bboxx1)/PS_SCALE, \
	  float(bboxy1)/PS_SCALE, float(bboxx2)/PS_SCALE, float(bboxy1)/PS_SCALE, float(bboxx2)/PS_SCALE,\
	  float(bboxy2)/PS_SCALE );
  fprintf(psfile, "%7.0f%7.0f lineto closepath gsave\n", float(bboxx1)/PS_SCALE, float(bboxy2)/PS_SCALE );
  fprintf(psfile, "gsave 1.0000 setgray fill grestore\n");
  fprintf(psfile, "clip newpath\n");
  
  if(incol!=0){
    psshade(psfile, 0.0, bakcol, rgb,  *maxcolor, incol);
    psubox(psfile, float(bboxx1), float(bboxy1), float(bboxx2), float(bboxy1), float(bboxx2), float(bboxy2),\
	   float(bboxx1), float(bboxy2));
  }
  
  
  return 1;
}






// -----------------------------------------------------------------------------
//
//    psclose  -  Write final lines to a PostScript file and close
//
// -----------------------------------------------------------------------------

int psclose(FILE * psfile, int bboxx1,int bboxx2,int bboxy1,int bboxy2){
  
  fprintf(psfile, "grestore stroke\n");
  fprintf(psfile, "PSLibSave restore\n");
  fprintf(psfile, "showpage\n");
  fprintf(psfile, "%sTrailer\n", "%%");
  fprintf(psfile, "%sBoundingBox:%10i%10i%10i%10i\n", "%%", bboxx1 - 1, bboxy1 - 1, bboxx2 + 1, bboxy2 + 1);
  fprintf(psfile, "%sEOF\n", "%%");
  
  fclose(psfile);
  
  return 1;
}






// -----------------------------------------------------------------------------
//
//    psbbox -  Write out bounded box to PostScript file
//
// -----------------------------------------------------------------------------

int psbbox(FILE * psfile, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4){
  
 
  
  fprintf(psfile, "%7.0f%7.0f%7.0f%7.0f%7.0f%7.0f%7.0f%7.0f Pline4\n", x1/PS_SCALE, y1/PS_SCALE, x2/PS_SCALE, \
	  y2/PS_SCALE, x3/PS_SCALE, y3/PS_SCALE, x4/PS_SCALE, y4/PS_SCALE);
  
  
  return 1;
}






// -----------------------------------------------------------------------------
//
//    psubox -  Write out unbounded box to PostScript file
//
// -----------------------------------------------------------------------------

int psubox(FILE * psfile, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4){
 
  
  fprintf(psfile, "%7.0f%7.0f%7.0f%7.0f%7.0f%7.0f%7.0f%7.0f Pl4", x1/PS_SCALE, y1/PS_SCALE, x2/PS_SCALE,\
	  y2/PS_SCALE, x3/PS_SCALE, y3/PS_SCALE, x4/PS_SCALE, y4/PS_SCALE);
  
  return 1;
} 






// -----------------------------------------------------------------------------
//
//  pstriangle - Write out bounded triangle to PostScript file
//
// -----------------------------------------------------------------------------

int pstriangle(FILE * psfile, float x1, float y1, float x2, float y2, float x3, float y3){
  
  fprintf(psfile, "%7.0f%7.0f%7.0f%7.0f%7.0f%7.0f Pline3", x1/PS_SCALE, y1/PS_SCALE, x2/PS_SCALE,\
	  y2/PS_SCALE, x3/PS_SCALE, y3/PS_SCALE);
  
  
  return 1;
}






// ----------------------------------------------------------------------------
//
//   psutriangle -  Write out unbounded triangle to PostScript file
//
// -----------------------------------------------------------------------------

int psutriangle(FILE * psfile, float x1, float y1, float x2, float y2, float x3, float y3){
  
  
  fprintf(psfile, "%7.0f%7.0f%7.0f%7.0f%7.0f%7.0f Pl3", x1/PS_SCALE, y1/PS_SCALE, x2/PS_SCALE,\
	  y2/PS_SCALE, x3/PS_SCALE, y3/PS_SCALE);
  
  
  return 1;
}


// -----------------------------------------------------------------------------
//
//  psx  -  Write out an X to a PostScript file
//
//------------------------------------------------------------------------------

int psx(FILE * psfile, float x, float y, float size, float lwidth){
  
  fprintf(psfile," gsave\n");
  fprintf(psfile,"/o {\n");
  fprintf(psfile, "%f setlinewidth\n", lwidth/PS_SCALE);
  fprintf(psfile,"0.000 setgray\n");
  fprintf(psfile,"newpath\n");
  fprintf(psfile,"translate\n");
  fprintf(psfile,"-%f -%f m\n", size/PS_SCALE, size/PS_SCALE);
  fprintf(psfile,"%f %f l\n", size/PS_SCALE, size/PS_SCALE);
  fprintf(psfile,"-%f %f m\n", size/PS_SCALE, size/PS_SCALE);
  fprintf(psfile,"%f -%f l\n", size/PS_SCALE, size/PS_SCALE);
  fprintf(psfile,"} bind def\n");
  fprintf(psfile,"%f %f o\n", x/PS_SCALE, y/PS_SCALE);
  fprintf(psfile,"stroke\n");
  fprintf(psfile,"grestore\n");
  
  
  return 1; 

}



// -----------------------------------------------------------------------------
//
//   psrgbcolorb  -  Write background colour level with rgb values
//
// -----------------------------------------------------------------------------

int psrgbcolorb(FILE * psfile, float r, float g, float b){
  fprintf(psfile, "%8.4f%8.4f%8.4f Col\n", r, g, b);
  return 1;
}






// -----------------------------------------------------------------------------
//
//   pscolorb  -  Write background colour level 
//                            basing on color number on rgb table
//
//------------------------------------------------------------------------------

int pscolorb(FILE * psfile, float * rgb, int color){
  fprintf(psfile, "%8.4f%8.4f%8.4f Col\n", rgb[3*color], rgb[3*color+1], rgb[3*color+2]);
  return 1;
}






// -----------------------------------------------------------------------------
//
//  psrgbcolor  -  Write colour level
//
// -----------------------------------------------------------------------------

int psrgbcolor(FILE * psfile, float r, float g, float b){
	fprintf(psfile, "gsave %8.4f%8.4f%8.4f Col\n", r, g, b);
	return 1;
}
	
	
	
	
	
	
// -----------------------------------------------------------------------------
//
//  pscolor  -  Write colour level basing on color number on rgb table
//
// -----------------------------------------------------------------------------

int pscolor(FILE * psfile, float * rgb, int color){
  fprintf(psfile, "gsave %8.4f%8.4f%8.4f Col\n", rgb[3*color], rgb[3*color+1], rgb[3*color+2]);
  return 1;
}






// -----------------------------------------------------------------------------
//
//  psdisk  -  Write out a full circle (disk) to PostScript file
//
// -----------------------------------------------------------------------------

int psdisk(FILE * psfile, float x, float y, float radius){
  
  float r;
  
  r=radius;
  
  fprintf(psfile, "%10.5f %10.5f %10.5f D\n", x/PS_SCALE, y/PS_SCALE, r/*/PS_SCALE*/);
  
  
  
  return 1;
}






// -----------------------------------------------------------------------------
//
//  pscircle  -  Write out a open circle to PostScript file
//
// -----------------------------------------------------------------------------

int pscircle(FILE * psfile, float x, float y, float radius){
  
  float r;
  
  r=radius;
 
  fprintf(psfile, "%7.0f%7.0f%7.0f Circle\n", x/PS_SCALE, y/PS_SCALE, r/PS_SCALE);
  
  return 1;
}






// -----------------------------------------------------------------------------
//
//  psctext  -  Write out centred text to PostScript file
//
// -----------------------------------------------------------------------------

int psctext(FILE * psfile, float x, float y, float size, char * text){
  
  fprintf(psfile, "%7.0f%7.0f moveto\n", x/PS_SCALE, y/PS_SCALE);
  fprintf(psfile, "(%s)\n%4.1f Center\n", text, size/PS_SCALE);
  fprintf(psfile, "(%s)\n%4.1f Print\n", text, size/PS_SCALE);
  
  
  return 1;
}






// -----------------------------------------------------------------------------
// 
// psline  -  Write line out to PostScript file
//
// -----------------------------------------------------------------------------

int psline(FILE * psfile, float x1, float y1, float x2, float y2){
  
  fprintf(psfile, "%7.0f%7.0f%7.0f%7.0f L\n", x1/PS_SCALE, y1/PS_SCALE, x2/PS_SCALE, y2/PS_SCALE);
  
  
  return 1;
}






// -----------------------------------------------------------------------------
//
//  pslwidth  -  Write line-width out to PostScript file
//
// -----------------------------------------------------------------------------

int pslwidth(FILE * psfile, float lwidth){
  /*
    if(lwidth<0.01) lwidth=0.01;
    if(lwidth>2.0) lwidth=2.0;
  */
  fprintf(psfile, "%5.2f setlinewidth\n", lwidth/PS_SCALE);
  
  return 1;
}






// -----------------------------------------------------------------------------
//
//  psmarker  -  Write point-marker to PostScript file
//  1 - box
//  2 - open circle
//  3 - full circle
//  4 - open triangle
//
// -----------------------------------------------------------------------------

int psmarker(FILE * psfile, float x, float y, int marker, float widthx, float widthy){
  
  
  
  if(marker==1)
    psbbox(psfile, x - widthx, y - widthy, x - widthx, y + widthy, x + widthx, y + widthy, x + widthx, y - widthy);
  else if(marker==2)
    pscircle(psfile, x, y, widthx);
  else if(marker==3)
    psdisk(psfile, x, y, widthx);
  else if(marker==4)
    pstriangle(psfile, x - widthx, y - 0.6 * widthy, x + widthx, y - 0.6 * widthy, x, y + 1.2 * widthy);
  
  
  return 1;
} 






// -----------------------------------------------------------------------------
//
//  psmarkers  -  Write n point-markers to PostScript file with 
//                            coordinates (x[], y[]) markers in PS
//                            file are in groups of 10 per line to 
//                            minimize disk space usage 
//
//  1 - box
//  2 - open circle
//  3 - full circle
//  4 - open triangle
//
// -----------------------------------------------------------------------------

int psmarkers(FILE * psfile, float * x, float * y, int n, int marker, float widthx, float widthy){

  float r;
  r=widthx;
  int i, j;
  fprintf(psfile, "/PT {MB 0 0 %5.2f FC ME} bind def\n", (widthx));
  for(i=0; i<=0.1*float(n); i++){
    
    
    for(j=0; j<10; j++)
      if( (10*i+j)<n ){
	fprintf(psfile, "%10.5f %10.5f PT ", x[10*i + j]/PS_SCALE, y[10*i + j]/PS_SCALE, marker);
	//printf("%10.5f%10.5f PT %10i%10i\n", x[10*i + j]/PS_SCALE, y[10*i + j]/PS_SCALE, marker, i, j);
		
      }
    fprintf(psfile, "\n");
  }
  
  
  return 1;
} 






// -----------------------------------------------------------------------------
//
//  psrotctext  -  Write out text centred and rotated through 90
//                        degrees to PostScript file
//
// -----------------------------------------------------------------------------

int psrotctext(FILE * psfile, float x, float y, float size, char * text){
  
  //fprintf(psfile, "/DejaVuSans findfont\n");
  //fprintf(psfile, "17.28 scalefont\n");
  //fprintf(psfile, "setfont\n");
  //fprintf(psfile, "90.000000 rotate\n");
  //fprintf(psfile, "%.2f %.2f moveto\n", x/PS_SCALE, y/PS_SCALE);
  //fprintf(psfile, "(%s) show\n", text);
  //fprintf(psfile, "grestore\n");
  
  fprintf(psfile, "gsave\n");
  fprintf(psfile, "(%s)\n%.2f CenterRot90 Rot90\n", text, size/PS_SCALE);
  fprintf(psfile, "(%s)\n%.2f CenterRot90 Rot90\n", text, size/PS_SCALE);
  fprintf(psfile, "(%s)\n%.2f Print\n", text, size/PS_SCALE);
  fprintf(psfile, "stroke\n");
  fprintf(psfile, "grestore\n");
  
  return 1;
} 






// -----------------------------------------------------------------------------
//
//  psrottext -  Write out text rotated through 90 degrees to
//                        PostScript file
//
// -----------------------------------------------------------------------------

int psrottext(FILE * psfile, float x, float y, float size, char * text){

  fprintf(psfile, "gsave\n");
  fprintf(psfile, "%7.0f%7.0f moveto\n", x/PS_SCALE, y/PS_SCALE);
  fprintf(psfile, "(%s)\n%4.1f UncenterRot90 Rot90\n", text, size/PS_SCALE);
  fprintf(psfile, "(%s)\n%4.1f Print\n", text, size/PS_SCALE);
  fprintf(psfile, "stroke\n");
  fprintf(psfile, "grestore\n");
  
  return 1;
} 






// -----------------------------------------------------------------------------
//
//  psdash  -  Switch dashed lines on/off 
//                    (the larger onoff value the longer the breake is)
//
// -----------------------------------------------------------------------------

int psdash(FILE * psfile, int onoff){

  if(onoff==0)
    fprintf(psfile, "[]  0 setdash");
  else 
    fprintf(psfile, " [%2i %2i ]  0 setdash", onoff, onoff);
  
  return 1;
}




// ----------------------------------------------------------------------------
//
//  psshade  -  Write shading level out to PostScript file
//
// ----------------------------------------------------------------------------

int psshade(FILE * psfile, float shade, int colour, float * rgb,  int mxcolr, int incol){
	
  if(incol==0)
    fprintf(psfile, "gsave%7.4f setgray\n", shade);
  else
    fprintf(psfile, "Col%02i", colour);
  
  
  return 1;
}






// -----------------------------------------------------------------------------
//
//  pstext  -  Write out text to PostScript file
//
// -----------------------------------------------------------------------------

int pstext(FILE * psfile, float x, float y, float size, char * text){

  /*
    if( (size<0.5)||(size>99.0) )size=1.0;
  */
  fprintf(psfile, "%7.0f%7.0f moveto\n", x/PS_SCALE, y/PS_SCALE);
  fprintf(psfile, "(%s)\n%4.1f Print\n", text, size/PS_SCALE);
  
  return 1;
}






// -----------------------------------------------------------------------------
//
//  psbtext  -  Write out bold text to PostScript file
//
// -----------------------------------------------------------------------------

int psbtext(FILE * psfile, float x, float y, float size, char * text){
	
  float t1=x;
  float t2=y - size / 4.0;
  
  fprintf(psfile, "%7.0f%7.0f moveto\n", t1/PS_SCALE, t2/PS_SCALE);
  fprintf(psfile, "(%s)\n%4.1f Bprint\n", text, size);
  
  return 1;
}






// -----------------------------------------------------------------------------
//
//  pssymbol  -  Write out symbol text to PostScript file
//
// ----------------------------------------------------------------------------- 


int pssymbol(FILE * psfile, float x, float y, float size, char * text){

  float t1=x;
  float t2=y - size / 4.0;
  
  fprintf(psfile, "%7.0f%7.0f moveto\n", t1/PS_SCALE, t2/PS_SCALE);
  fprintf(psfile, "(%s)\n%4.1f Gprint\n", text, size);
  
  return 1;
}


// -----------------------------------------------------------------------------
//
//  pspath  -  Draw a path to PostScript file
//
// ----------------------------------------------------------------------------- 


int pspath(FILE * psfile, float * points, int n, float lwidth, int path_no){

  fprintf(psfile, "gsave\n");
  fprintf(psfile, "/p%i {\n", path_no);
  fprintf(psfile, "newpath\n");
  //fprintf(psfile, "translate\n");
  fprintf(psfile, "%.2f %.2f m\n", points[0]/PS_SCALE, points[1]/PS_SCALE);
  for(int i=1; i<n; i++)
    fprintf(psfile, "%.2f %.2f l\n", points[2*i]/PS_SCALE, points[2*i+1]/PS_SCALE);
  
  fprintf(psfile, "} bind def\n");
  fprintf(psfile, "%f setlinewidth\n", lwidth);

  fprintf(psfile, "0 setlinecap\n");
  //args for translate
  //fprintf(psfile, "0 0 p%i\n", path_no);
  fprintf(psfile, "p%i\n", path_no);
  fprintf(psfile, "stroke\n");
  fprintf(psfile, "grestore\n");
  
  return 1;
}


// -----------------------------------------------------------------------------
//
//  psfilledpath  -  Draw a filled path (filled contour) to PostScript file
//
// ----------------------------------------------------------------------------- 


int psfilledpath(FILE * psfile, float * points, int n, int path_no, float r, float g, float b){
  
  fprintf(psfile, "gsave\n");
  fprintf(psfile, "/p%i {\n", path_no);
  fprintf(psfile, "newpath\n");
  //fprintf(psfile, "translate\n");
  fprintf(psfile, "%.2f %.2f m\n", points[0]/PS_SCALE, points[1]/PS_SCALE);
  for(int i=1; i<n; i++)
    fprintf(psfile, "%.2f %.2f l\n", points[2*i]/PS_SCALE, points[2*i+1]/PS_SCALE);
  
  fprintf(psfile, "} bind def\n");
  fprintf(psfile, "0 setlinecap\n");
  fprintf(psfile, "%f %f %f setrgbcolor\n", r, g, b);
  fprintf(psfile, "gsave\n");
  //args for translate
  //fprintf(psfile, "0 0 p%i\n", path_no);
  fprintf(psfile, "p%i\n", path_no);
  fprintf(psfile, "fill\n");
  fprintf(psfile, "stroke\n");
  fprintf(psfile, "grestore\n");
  
  return 1;
}
