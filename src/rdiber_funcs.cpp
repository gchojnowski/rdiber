/*    rdiper_funcs.cpp: main modules   */

//    Copyright (C) 2009 Grzegorz Chojnowski
//
//    This file is part of DIBER.
//
//    DIBER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    DIBER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with DIBER.  If not, see <http://www.gnu.org/licenses/>.



#include "rdiber_funcs.h"
#include  "pslib.h"

using namespace std;




vector<string> split_into_words(const string& s, const string& delim, const bool keep_empty = true) {
    vector<string> result;
    if (delim.empty()) {
        result.push_back(s);
        return result;
    }
    string::const_iterator substart = s.begin(), subend;
    while (true) {
        subend = search(substart, s.end(), delim.begin(), delim.end());
        string temp(substart, subend);
        if (keep_empty || !temp.empty()) {
            result.push_back(temp);
        }
        if (subend == s.end()) {
            break;
        }
        substart = subend + delim.size();
    }
    return result;
}

//Extras...

std::vector<double> diber_dataset::get_svm_prob_estimates(){
  return prob_estimates_;
}


int diber_dataset::set_svm_prob_estimates(double *prob_estimates){
  prob_estimates_.resize(3, 0.0);
  for(int i=0; i<3; i++)
    prob_estimates_[i]=prob_estimates[i];
  return 1;
}

//------------------------------------------------------------------------------
//
//	DIBER_DATASET: mtzio 
//
//------------------------------------------------------------------------------


Diber_Error* diber_dataset::list_mtzfile_columns(char * hklin_fname, vector<clipper::String> * column_labels){

  clipper::CCP4MTZfile input_mtzfile;
  
  hklin_fname_ = hklin_fname;
  CMtz::MTZ* mtzin = CMtz::MtzGet( hklin_fname, 0 );
  if( mtzin == NULL )
    return local_error_ = new Diber_Error( (char*)"ERROR: Cannot find input file", 1);
  try{
    input_mtzfile.open_read( hklin_fname );
  } catch ( ... ){
    return local_error_ = new Diber_Error( (char*)"ERROR: Cannot find input file", 1);
  }  

  vector<clipper::String> column_labels_tmp = input_mtzfile.column_labels();
  *column_labels = column_labels_tmp;
  
  input_mtzfile.close_read();
  

  return local_error_ = new Diber_Error( (char*)"Normal termination", 0);
}



//-----------------------------



Diber_Error* diber_dataset::read_mtzfile(char * hklin_fname, char * FP_label, char * SIGFP_label){

  clipper::CCP4MTZfile input_mtzfile;
   
  hklin_fname_ = hklin_fname;

  try{
    input_mtzfile.open_read( hklin_fname );
  } catch (...){
    return local_error_ = new Diber_Error( (char*)"ERROR: Cannot find input file", 1);
  }
  

  labin_=string("*/*/[")+string(FP_label)+string(" ")+string(SIGFP_label)+string("]");

  input_mtzfile.import_hkl_info( (*input_hkls_) );
  


  input_hkldata_fsigf_->init( (*input_hkls_), input_hkls_->cell() );
  

  try{
    input_mtzfile.import_hkl_data( (*input_hkldata_fsigf_), labin_.c_str() );
    
  }catch (...){
    return local_error_ = new Diber_Error( (char*)"ERROR: Missing column", 1);
  }

  input_mtzfile.close_read();



  //----------------  Clippers anisotropy correction  -----------------------

  try{
    clipper_anisotropy_correction( *input_hkldata_fsigf_, shell_omit_invresolsq_min_, shell_omit_invresolsq_max_ );
  } catch (...){
    return local_error_ = new Diber_Error( (char*)"ERROR: Problem with anisotropy correction.", 1);
  }
  
  //----------------  Clippers anisotropy correction  -----------------------
 


  oneovercrtVol_=pow( input_hkls_->cell().volume(), -1./3. );

 


  printf("\n ------------------------------------------------------------------------------\n"
	"  %s Input data\n\n", program_mode_ );
 


  printf("    Input mtz file name: %s\n", hklin_fname);
  printf("    Column name:  %s\n", labin_.c_str());
  printf("    Space-group name: %10s\n", input_hkls_->spacegroup().symbol_hm().c_str());
  printf("    Unit cell: %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f\n",\
	 get_hkls().cell().a(),\
	 get_hkls().cell().b(),\
	 get_hkls().cell().c(), \
	 get_hkls().cell().alpha_deg(),\
	 get_hkls().cell().beta_deg(),\
	 get_hkls().cell().gamma_deg() );
  printf("    Resolution range: %7.2f %7.2f\n",\
	 sqrt(1.0/get_hkldata_fsigf().invresolsq_range().max()),\
	 sqrt(1.0/get_hkldata_fsigf().invresolsq_range().min()));

  
  return local_error_ = new Diber_Error( (char*)"Normal termination", 0);
  
}



// ---------------------------------------------------------------------------
//
//             DIBER_AVERAGES: Progress bar
//
//----------------------------------------------------------------------------\

diber_scores::progress_bar::~progress_bar(void){cout<<"| DONE\n\n\n"<<flush;}

diber_scores::progress_bar::progress_bar(int width){
  total_width=width;
  chunk_size=1.0/(double)(total_width-2);
  chunk_no=0;
  cout<<"\n\n 0%"<<flush;
  for (int i=0; i<total_width-3; i++)
    cout<<" "<<flush;
  cout<<"100%"<<endl<<" |"<<flush;
}



int diber_scores::progress_bar::progress_bar_increase(double total_progress){
  if (total_progress>(double)(chunk_no)*chunk_size){
    cout<<"="<<flush;
    chunk_no++;
  }

}






// ---------------------------------------------------------------------------
//
//  DIBER: Estimate completyness of the dataset in a 3.1 - 3.65 \AA shell
//
//----------------------------------------------------------------------------\


// Diber_Error* diber_scores::estimate_completness(){

// for ( clipper::HKL_info::HKL_reference_index ih = hkldata_shell_omit->first(); !ih.last(); ih.next() )
//       if( !contains(invresolsq_min, (invresolsq=ih.invresolsq()), invresolsq_max) )
// 	(*hkldata_shell_omit)[ih] = input_diber_dataset->get_hkldata_fsigf()[ih]; 

// }



// ---------------------------------------------------------------------------
//
//             DIBER_AVERAGES: Calculate E-values
//
//----------------------------------------------------------------------------\

Diber_Error* diber_scores::calculate_Evalues(){
  const int nprm = 12;  

  // create initial E-datasets
  try{

    

    //SHELL OMIT SHELL OMIT SHELL OMIT SHELL OMIT SHELL OMIT 
    float invresolsq_min = input_diber_dataset->get_shell_omit_invresolsq_min();
    float invresolsq_max = input_diber_dataset->get_shell_omit_invresolsq_max();
    float invresolsq;
    

    clipper::HKL_data< clipper::datatypes::F_sigF<float> >* hkldata_shell_omit;
    clipper::HKL_data< clipper::datatypes::E_sigE<float> >* hkldata_esige_originalspg_shell_omit;


    hkldata_shell_omit = new clipper::HKL_data<clipper::datatypes::F_sigF<float> >;
    hkldata_esige_originalspg_shell_omit =  new clipper::HKL_data<clipper::datatypes::E_sigE<float> >;
    hkldata_shell_omit->init( input_diber_dataset->get_hkls(), input_diber_dataset->get_hkls().cell() );

    
   


    for ( clipper::HKL_info::HKL_reference_index ih = hkldata_shell_omit->first(); !ih.last(); ih.next() )
      if( !contains(invresolsq_min, (invresolsq=ih.invresolsq()), invresolsq_max) )
	(*hkldata_shell_omit)[ih] = input_diber_dataset->get_hkldata_fsigf()[ih]; 


    //OMIT
    hkldata_esige_originalspg_shell_omit->init (input_diber_dataset->get_hkls(), input_diber_dataset->get_hkls().cell());
    (*hkldata_esige_originalspg_shell_omit).compute( (*hkldata_shell_omit), clipper::datatypes::Compute_EsigE_from_FsigF<float>() );
    
   
    //ORIG
    hkldata_esige_originalspg->init ( input_diber_dataset->get_hkls(), input_diber_dataset->get_hkls().cell());
    (*hkldata_esige_originalspg).compute( input_diber_dataset->get_hkldata_fsigf(), clipper::datatypes::Compute_EsigE_from_FsigF<float>() );
    

    


    // calculate scaling factors
    std::vector<double> params_init( nprm, 1.0 );
    clipper::BasisFn_spline bspline_basis_function( *hkldata_esige_originalspg_shell_omit, nprm, 2.0 );
    clipper::TargetFn_scaleEsq<clipper::datatypes::E_sigE<float> > target_function( *hkldata_esige_originalspg_shell_omit );
    clipper::ResolutionFn escale( input_diber_dataset->get_hkls(), bspline_basis_function, target_function, params_init );
    

    // scale structure factors
    for ( clipper::HKL_info::HKL_reference_index ih = hkldata_esige_originalspg->first(); !ih.last(); ih.next() )
      if ( !(*hkldata_esige_originalspg)[ih].missing() ) (*hkldata_esige_originalspg)[ih].scale( sqrt( escale.f(ih) ) );
  } catch(...){
    return local_error_ = new Diber_Error( (char*)"ERROR: Calculation of E-values failed.", 1);
  }
  

  return local_error_ = new Diber_Error( (char*)"Normal termination", 0);
}




// ---------------------------------------------------------------------------
//
//          DIBER_AVERAGES: expand dataset to P1 
//
//----------------------------------------------------------------------------\

Diber_Error* diber_scores::expand_to_P1(){
 
  hkls_p1->init( clipper::Spacegroup( clipper::Spgr_descr( 1 ) ),\
		 input_diber_dataset->get_hkls().cell(),input_diber_dataset->get_hkls().resolution() );
    
  hkldata_esige_p1->init( input_diber_dataset->get_hkls(), input_diber_dataset->get_hkls().cell() );
    
  for ( clipper::HKL_info::HKL_reference_index ih = hkldata_esige_p1->first(); !ih.last(); ih.next() )
      (*hkldata_esige_p1)[ih] = (*hkldata_esige_originalspg)[ih.hkl()]; 


  return local_error_ = new Diber_Error( (char*)"Normal termination", 0);

}






// ---------------------------------------------------------------------------
//
//          DIBER_AVERAGES: icosahedral covering file read in (ASU ONLY!!!)          
//
//----------------------------------------------------------------------------

Diber_Error* diber_scores::read_icosahedral_covering_data(){


  Diber_Error* local_error;

  //used only for asu checking...
  clipper::HKL fake_hkl;
  double theta, delta;
  clipper::Vec3<double> v_ortho_tmp;
  string string_comment_line;
  
	  
  vector<delta_theta> ico_covering;
  ifstream icofile;

  //if input_diber_dataset->get_hkls() is not initialized the procedure will crash!!!
  

  try{
    icofile.open((string(getenv("RDIBER")) + "/data/data_ico.dat").c_str());
    
    //skip comment line
    getline(icofile, string_comment_line);

    input_diber_dataset->get_icodata().clear();
    delta_theta * delta_theta_tmp;
    while(icofile){
      icofile>>theta;
      icofile>>delta;
      if(icofile.eof()) break;
      
      theta*=M_PI/180.0;
      delta*=M_PI/180.0;


      //ASU test...
      v_ortho_tmp[0] = 1000.0*cos(theta)*cos(delta);
      v_ortho_tmp[1] = 1000.0*sin(theta)*cos(delta);
      v_ortho_tmp[2] = 1000.0*sin(delta);
      v_ortho_tmp=v_ortho_tmp;
      

      
      fake_hkl=((clipper::Coord_reci_orth(v_ortho_tmp)).coord_reci_frac(input_diber_dataset->get_hkls().cell())).hkl();

      
      if(input_diber_dataset->get_hkls().spacegroup().recip_asu(fake_hkl)){
	delta_theta_tmp = new delta_theta;
	delta_theta_tmp->theta=theta;
	delta_theta_tmp->delta=delta;
	delta_theta_tmp->average_intensity=-3.0;
	delta_theta_tmp->npoints=-3;
	input_diber_dataset->get_icodata().push_back(*delta_theta_tmp);
      }
    }		
	
    icofile.close();
    
    
  } catch(...){
    return local_error =						\
      new Diber_Error( (char*)"ERROR: Unable to open data_ico.dat file - check your installation!", 1);
  }



 


  return local_error = new Diber_Error( (char*)"Normal termination", 0);

}





//------------------------------------------------------------------------------
//
//	DIBER_AVERAGES: calculate average intensity at given orientation
//
//------------------------------------------------------------------------------

int diber_scores::calculate_single_average(double theta_given, double delta_given,\
		   double * average_intensity, double * points_used){

  //double hwhm_R_local   = 0.09;
  //double hwhm_zs_local  = 0.02;

  //XXX
  double hwhm_R_local   = _hwhm_R;
  double hwhm_zs_local  = _hwhm_zs;



  //XXX
  //double LA_resol_local = 1.0/3.4;
  //double LA_resol_local = 1.0/3.2;
  double LA_resol_local = 1.0/_shell_resolution;
  

	
  double h_range_ortho, k_range_ortho, l_range_ortho;
  double h_axis_angle, k_axis_angle, l_axis_angle;
  double d_longit = 0.0;
  double d_lateral = 0.0;
  
  int h_range, k_range, l_range;
  int used_index;
  double sum;

  clipper::Vec3<double> v_ortho_normal;
  clipper::Vec3<double> v_ortho_normal_zero;
  clipper::Vec3<double> v_ortho_tmp;
  clipper::Vec3<double> k_ortho;
  clipper::Vec3<int>    v_miller;

  clipper::HKL hkl_zero;
  clipper::HKL hkl_one;


  

  
  v_ortho_tmp[0] = cos(theta_given)*cos(delta_given);
  v_ortho_tmp[1] = sin(theta_given)*cos(delta_given);
  v_ortho_tmp[2] = sin(delta_given);
  
  v_ortho_normal_zero=v_ortho_tmp;
  v_ortho_tmp=(LA_resol_local)*v_ortho_tmp;
  
  
  //the nearest hkl at 3.4A corresponding to a given direction, used for
  //ROUGH estimate of RS summation ranges...
  hkl_zero=((clipper::Coord_reci_orth(v_ortho_tmp)).coord_reci_frac(input_diber_dataset->get_hkls().cell())).hkl();
  
  
  //l_range...
  v_ortho_normal[0]=0.0;
  v_ortho_normal[1]=0.0;
  v_ortho_normal[2]=1.0;
    
 
  l_axis_angle = acos(  clipper::Vec3<double>::dot(v_ortho_normal, v_ortho_normal_zero)  );
  l_range_ortho=hwhm_R_local*sin(l_axis_angle);

  if(l_range_ortho<hwhm_zs_local) l_range_ortho=hwhm_zs_local;
  l_range = (int)(ceil( (clipper::Coord_reci_orth( 0.0, 0.0, fabs(l_range_ortho) )).coord_reci_frac(input_diber_dataset->get_hkls().cell()).ws() ));
  
  //k_range
  v_ortho_normal[0]=0.0;
  v_ortho_normal[1]=1.0;
  v_ortho_normal[2]=0.0;
    
 
  k_axis_angle = acos(  clipper::Vec3<double>::dot(v_ortho_normal, v_ortho_normal_zero)  );
  k_range_ortho=hwhm_R_local*sin(k_axis_angle);

  if(k_range_ortho<hwhm_zs_local) k_range_ortho=hwhm_zs_local;
  k_range = (int)(ceil( (clipper::Coord_reci_orth( 0.0, fabs(k_range_ortho), 0.0 )).coord_reci_frac(input_diber_dataset->get_hkls().cell()).vs() ));
  
  //h_range
  v_ortho_normal[0]=1.0;
  v_ortho_normal[1]=0.0;
  v_ortho_normal[2]=0.0;
  
 
  h_axis_angle = acos(  clipper::Vec3<double>::dot(v_ortho_normal, v_ortho_normal_zero)  );
  h_range_ortho=hwhm_R_local*sin(h_axis_angle);

  if(h_range_ortho<hwhm_zs_local) h_range_ortho=hwhm_zs_local;
  h_range = (int)(ceil( (clipper::Coord_reci_orth( fabs(h_range_ortho), 0.0, 0.0 )).coord_reci_frac(input_diber_dataset->get_hkls().cell()).us() ));
  
  
  
  used_index=0;
  sum=0.0;

  for(int ih=-h_range; ih<=h_range; ih++)
    for(int ik=-k_range; ik<=k_range; ik++)
      for(int il=-l_range; il<=l_range; il++){
	
	v_miller[0] = hkl_zero.h() + ih;
	v_miller[1] = hkl_zero.k() + ik;
	v_miller[2] = hkl_zero.l() + il;
	hkl_one=(clipper::Coord_reci_frac(v_miller[0], v_miller[1], v_miller[2])).hkl();
	
	
      
	
	d_longit = (hkl_one.coord_reci_orth(input_diber_dataset->get_hkls().cell()))*(v_ortho_normal_zero);
	k_ortho = (hkl_one.coord_reci_orth(input_diber_dataset->get_hkls().cell())) - d_longit*v_ortho_normal_zero;
	
	d_lateral = sqrt(k_ortho*k_ortho);
	d_longit-=LA_resol_local;
	
	
	
	
	if( (fabs(d_longit)<=hwhm_zs_local) && (fabs(d_lateral)<=hwhm_R_local) ){
	  if(!input_diber_dataset->get_hkls().spacegroup().hkl_class(hkl_one).sys_abs()){ 
	    if(!(*hkldata_esige_p1)[hkl_one].missing()){						
	      used_index++;
	      sum+=(*hkldata_esige_p1)[hkl_one].E()*(*hkldata_esige_p1)[hkl_one].E();
	    }
	  }
	}
      }

  *points_used=(double)(used_index);

  if(used_index>0)
    *average_intensity=sum/(double)(used_index);
  else
    *average_intensity=0.0;

  return 1;

}



//------------------------------------------------------------------------------
//
//	DIBER_AVERAGES: fill in averages vector
//
//------------------------------------------------------------------------------


Diber_Error* diber_scores::calculate_all_averages(){
  Diber_Error* local_error;

  //prepare datasaet
  
  if( (local_error = calculate_Evalues())->Number() ) return local_error;
  if( (local_error = expand_to_P1())->Number() ) return local_error;
  if( (local_error = read_icosahedral_covering_data())->Number() ) return local_error;


  printf("\n ------------------------------------------------------------------------------\n"
	"  %s Input cards\n\n", input_diber_dataset->get_program_mode() );


  

  printf("    %s will calculate %5i average intensities.\n", input_diber_dataset->get_program_mode(), int(input_diber_dataset->get_icodata().size()));

  
  
  double average_intensity_tmp;
  double npoints_tmp;
  float max_average_tmp=-3.0;
  float max_average_L = 0.0; 
  
  progress_bar * draw_progress_bar = new progress_bar(73);
  
  
  for(int iq=0; iq<input_diber_dataset->get_icodata().size(); iq++){

    calculate_single_average(input_diber_dataset->get_icodata()[iq].theta, input_diber_dataset->get_icodata()[iq].delta,\
    	 &average_intensity_tmp, &npoints_tmp);
    
    input_diber_dataset->get_icodata()[iq].average_intensity=average_intensity_tmp;
    input_diber_dataset->get_icodata()[iq].npoints=npoints_tmp;
   
    (draw_progress_bar)->progress_bar_increase((double)iq/(double)input_diber_dataset->get_icodata().size());
    if(max_average_tmp<average_intensity_tmp){
    	max_average_tmp = average_intensity_tmp;
	max_average_L   = npoints_tmp;
    }
  }	


  input_diber_dataset->set_max_average(max_average_tmp, max_average_L);
 


  delete(draw_progress_bar);


  return local_error = new Diber_Error( (char*)"Normal termination", 0);
}


//------------------------------------------------------------------------------
//
//	DIBER_AVERAGES: run PHASER and get the highest LLG
//
//------------------------------------------------------------------------------



Diber_Error* diber_scores::run_phaser(char * hklin_fname, char * FP_label, string phaser_root){
  
  

  
  printf ("\n ------------------------------------------------------------------------------\n"
	  "  DIBER Running prepared PHASER\n\n" );


  vector<string> words;



  stringstream phaser_script;
  phaser_script.str("");

  //empty PHASER root, let's fix it!
  if(phaser_root.size()==0){
    words = split_into_words(hklin_fname, ".mtz", false);
    phaser_root=words[0];
  }
 
  

  phaser_script<<getenv("CBIN")<<"/phaser << phaser_eof\n"\
	       <<"MODE MR_FRF\n"\
	       <<"HKLIN "<<hklin_fname<<"\n"\
	       <<"LABIN F="<<FP_label<<"\n"\
               <<"COMPosition percentage  50\n"\
	       <<"ENSEMBLE BDNA PDBFILE "<<getenv("RDIBER")<<"/data/fiber_11bp.pdb"<<" RMS 0.5\n"\
	       <<"RESOLUTION 3.5332 3.0957\n"\
	       <<"SEARCH ENSEMBLE BDNA\n"\
	       <<"FINAL ROT SELECT NUM 10\n"\
	       <<"SAMPling ROT 20.0\n"\
	       <<"RESCORE ROT ON\n"\
	       <<"ROOT "<<phaser_root<<"\n"\
	       <<"VERBOSE OFF\n"\
	       <<"OUTLier REJECT OFF\n"\
	       <<"MUTE OFF\n"\
	       <<"phaser_eof";

  
  
  system(phaser_script.str().c_str());


  char mystring[255];
  ifstream phaser_output_file;


 
  

  //------------------ find Top FRF LLG in an output file -----------------------

  phaser_output_file.open(( phaser_root + ".sum").c_str());
  
  while(phaser_output_file){
    phaser_output_file.getline(mystring, 255);
    words = split_into_words(mystring, " ", false);
    if(words.size()>0)
    // Check if the Phaser output.sum is in an old or a new format...
    if (words[0].compare("#SET")==0){
        //it's new :)
        if( (words[1].compare("Top")==0) and (words[3].compare("Second")==0) and (words[5].compare("Third")==0)){
            phaser_output_file.getline(mystring, 255);
            words = split_into_words(mystring, " ", false);
            input_diber_dataset->set_phaser_max_llg( atof(words[1].c_str()) );
        }
    } else if( (words[0].compare("Top")==0) and (words[2].compare("Second")==0) and (words[4].compare("Third")==0)){
        // this must be an old format
        phaser_output_file.getline(mystring, 255);
        words = split_into_words(mystring, " ", false);
        input_diber_dataset->set_phaser_max_llg( atof(words[0].c_str()) );
    }
  }
  phaser_output_file.close();
  
  if( clipper::Util::is_nan( input_diber_dataset->get_phaser_max_llg() ) )
    return local_error_ = new Diber_Error( (char*)"Couldn't find LLG info if Phaser log-file.", 0);



  //--------------- find Top (Ten?) RF solutions in an output file -------------


  printf("\n ------------------------------------------------------------------------------\n"
	 "  DIBER Top ten Phaser solutions (spherical coordinates):\n\n");
  input_diber_dataset->get_phaser_solutions().clear();
  phaser_output_file.open(( phaser_root + ".rlist").c_str());
  while(phaser_output_file){
    phaser_output_file.getline(mystring, 255);
    words = split_into_words(mystring, " ", false);
   
    if(words.size()>6)
      if( (words[0].compare("SOLU")==0) and (words[3].compare("BDNA")==0) and (words[4].compare("EULER")==0)){
	
	printf("    THETA %7.3f PHI %7.3f\n", atof(words[5].c_str()), atof(words[6].c_str()) );
	input_diber_dataset->get_phaser_solutions().push_back( atof(words[5].c_str())*M_PI/180.0 );
	input_diber_dataset->get_phaser_solutions().push_back( atof(words[6].c_str())*M_PI/180.0 );
	input_diber_dataset->get_phaser_solutions().push_back( atof(words[7].c_str())*M_PI/180.0 );
      }
  }
  phaser_output_file.close();



  return local_error_ = new Diber_Error( (char*)"Normal termination", 0);

}


//*******************************************************************************
//************************ GENERAL OUTPUT (PS&STD) ******************************
//*******************************************************************************



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//
//	DIBER_OUTPUT
//
//------------------------------------------------------------------------------


int diber_output::draw_horizontal_bar(float pbty){
  
  cout<<"  |";
  for(float xx=.025; xx<=1.0; xx+=.025)
    if(xx<pbty)
      cout<<"=";
    else
      cout<<" ";
  printf("|   %5.0f\%%\n", pbty*100.0);

}



//------------------------------------------------------------------------------


Diber_Error* diber_output::create_output(char * psfilename){

  Diber_Error* local_error;
  ps_graph * graph;


  if(strlen(psfilename)>0){
    
    if( (local_error = open_new_postscript_file( psfilename, clipper::Util::is_nan( diber_dataset_input_->get_phaser_max_llg() )))->Number())
      return local_error;
    if( (local_error = play_with_svm_phaser_enhanced(psfile_))->Number()) 
      return local_error;
    
    
    close_postscript_file();
  }
  else  
    if( (local_error = play_with_svm_phaser_enhanced(NULL))->Number()) return local_error;  
  
  
  return local_error = new Diber_Error( (char*)"Normal termination", 0);
}


//------------------------------------------------------------------------------


Diber_Error* diber_output::open_new_postscript_file(char * psfilename, int legend){
  Diber_Error* local_error;
  
  psfilename_ = psfilename;
  rgb_=set_rgb(&maxcolor_);
  psfile_ = psopen(psfilename, 50, 530, 50, 780, &maxcolor_, rgb_, 1, 1, 1, 1, 1);
  if(psfile_==NULL) local_error = new Diber_Error((char*)"ERROR: Unable to open postscript output file!", 1 );
  

  time_t rawtime;
  time ( &rawtime );

  
  vector<string> words = split_into_words(diber_dataset_input_->get_hklin_fname(), "/");
  
  //Here we add a page title...
  stringstream title_str;
  title_str.str("");
  if(diber_dataset_input_->use_diber())
  	title_str<<"DIBER output (HKLIN: "<<words[words.size()-1]<<")";
  else
	title_str<<"RIBER output (HKLIN: "<<words[words.size()-1]<<")";
  pstext(psfile_, 105, 760, 20.0,  (char*)title_str.str().c_str());
  title_str.str("");
  title_str<<"Run time: "<<ctime(&rawtime);
  pstext(psfile_, 200, 745, 10.0,  (char*)title_str.str().c_str());



  // ...and a legend.
  if(legend){
    title_str.str("");
    if(diber_dataset_input_->use_diber())
	title_str<<"Colorcodes: complexes (red), proteins (green), DNA only (blue). Background color levels, from light to dark, corresponds";
    else
	title_str<<"Colorcodes: complexes (red), proteins (green), RNA only (blue). Background color levels, from light to dark, corresponds";
	
    pstext(psfile_, 80, 65, 9.0,  (char*)title_str.str().c_str());
    title_str.str("");
    if(diber_dataset_input_->use_diber())
    	title_str<<"to a classification probability above 80%, 90% and 95% respectively. Your dataset is marked with \"X\".";
    else
	title_str<<"to a classification probability above 50% and 80% respectively. Your dataset is marked with \"X\".";
    pstext(psfile_, 80, 55, 9.0,  (char*)title_str.str().c_str());
  }else{
    title_str.str("");
    title_str<<"Top ten Phaser rotation function solutions are marked on a projection with \"X\".";
    pstext(psfile_, 80, 55, 9.0,  (char*)title_str.str().c_str());
  }


  return local_error = new Diber_Error( (char*)"Normal termination", 0);
}


//------------------------------------------------------------------------------



Diber_Error* diber_output::close_postscript_file(){
  Diber_Error* local_error;

  psclose(psfile_, 50, 530, 50, 780);
  return local_error = new Diber_Error( (char*)"Normal termination", 0);
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//
//	DIBER_OUTPUT :: PS_GRAPH
//
//------------------------------------------------------------------------------

int diber_output::ps_graph::draw_line(float x1, float y1, float x2, float y2, float lwidth){
  pscolor(psfile__, rgb_, 0);
  pslwidth(psfile__, lwidth);
  double local_xwidth = local_xmax_-local_xmin_;
  double local_ywidth = local_ymax_-local_ymin_;
  
  psline(psfile__, ps_lowercornerx_+ps_xwidth_*(x1-local_xmin_)/(local_xwidth),\
	 ps_lowercornery_+ps_ywidth_*(y1-local_ymin_)/(local_ywidth),\
	 ps_lowercornerx_+ps_xwidth_*(x2-local_xmin_)/(local_xwidth),\
	 ps_lowercornery_+ps_ywidth_*(y2-local_ymin_)/(local_ywidth));

  return 1;
}


int diber_output::ps_graph::draw_marker(float x, float y, float size, float lwidth){
  pscolor(psfile__, rgb_, 0);
  pslwidth(psfile__, lwidth);
  float local_xwidth = local_xmax_-local_xmin_;
  float local_ywidth = local_ymax_-local_ymin_;

  if( contains(local_xmin_, x, local_xmax_) && contains(local_ymin_, y, local_ymax_) )
    psx(psfile__, ps_lowercornerx_+ps_xwidth_*(x-local_xmin_)/(local_xwidth),\
	ps_lowercornery_+ps_ywidth_*(y-local_ymin_)/(local_ywidth), size, lwidth);
  
 
  return 1;
}



//------------------------------------------------------------------------------



int diber_output::ps_graph::add_labels_dirty(){

  //WARNING: ough.. that's a really ugly piece of code...

  float xlabel_xpos = ps_lowercornerx_ + 0.5*ps_xwidth_-30;
  float xlabel_ypos = ps_lowercornery_ - 28;


  float ylabel_xpos = ps_lowercornerx_ - 18;
  float ylabel_ypos = ps_lowercornery_ +0.5*ps_ywidth_-70;
 
  float scale=0.8;

  //X label
  fprintf(psfile__, "gsave");
  fprintf(psfile__, "/sqrt3 {\n");
  fprintf(psfile__, "translate\n");
  
  fprintf(psfile__, "/Cmr10 findfont\n");
  fprintf(psfile__, "%f scalefont\n",17.28*scale);
  fprintf(psfile__, "setfont\n");

  fprintf(psfile__, "0.0 0.0 m\n");
  fprintf(psfile__, "/one glyphshow\n");
  
  fprintf(psfile__, "/Cmmi10 findfont\n");
  fprintf(psfile__, "%f scalefont\n", 17.28*scale);
  fprintf(psfile__, "setfont\n");
  fprintf(psfile__, "%f 0.0 m\n", 8.632812*scale);
  fprintf(psfile__, "/slash glyphshow\n");
  
  fprintf(psfile__, "/Cmr10 findfont\n");
  fprintf(psfile__, "%f scalefont\n", scale*9.4672);
  fprintf(psfile__, "setfont\n");
  fprintf(psfile__, "%f %f m\n", 17.265624*scale, 7.818750*scale);
  fprintf(psfile__, "/three glyphshow\n");
  
  fprintf(psfile__, "/Cmex10 findfont\n");
  fprintf(psfile__, "%f scalefont\n", 11.0195*scale);
  fprintf(psfile__, "setfont\n");
  fprintf(psfile__, "%f %f  m\n", 15.987890*scale, 12.593750*scale);
  fprintf(psfile__, "/radicalbig glyphshow\n");
 
  fprintf(psfile__, "%f %f %f %f  rectfill\n",\
	  27.003515*scale, 11.871250*scale, 14.681053*scale, 1.080000*scale );
  fprintf(psfile__, "/Cmmi10 findfont\n");
  fprintf(psfile__, "%f scalefont\n",17.28*scale);
  fprintf(psfile__, "setfont\n");

  fprintf(psfile__, "%f %f m\n", 29.163515*scale, 0.326250*scale);
  fprintf(psfile__, "/V glyphshow\n");
  fprintf(psfile__, "} bind def\n");
  fprintf(psfile__, "%f %f sqrt3\n", xlabel_xpos, xlabel_ypos);
  fprintf(psfile__, "stroke\n");
  fprintf(psfile__, "grestore\n");


  //Y label
  pscolor(psfile__, rgb_, 0);
  psrottext(psfile__, ylabel_xpos, ylabel_ypos, 12, (char*)"The strongest average E");
  psrottext(psfile__, ylabel_xpos-6, ylabel_ypos+116, 8, (char*)"2");




  return 1;
}


//------------------------------------------------------------------------------



int diber_output::ps_graph::draw_frame(float lwidth, float tickstep_x, float tickstep_y){
  stringstream string_txt;
  pscolor(psfile__, rgb_, 0);
  pslwidth(psfile__, lwidth);
  //draw box
  psline(psfile__, ps_lowercornerx_, ps_lowercornery_, ps_lowercornerx_+ps_xwidth_, ps_lowercornery_);
  psline(psfile__, ps_lowercornerx_+ps_xwidth_, ps_lowercornery_,\
	 ps_lowercornerx_+ps_xwidth_, ps_lowercornery_+ps_ywidth_);
  psline(psfile__, ps_lowercornerx_+ps_xwidth_, ps_lowercornery_+ps_ywidth_,\
	 ps_lowercornerx_, ps_lowercornery_+ps_ywidth_);
  psline(psfile__, ps_lowercornerx_, ps_lowercornery_+ps_ywidth_, ps_lowercornerx_, ps_lowercornery_);





  float tick_length=3;

  //add X_ticks w labels
  for(float ff=local_xmin_; ff<=local_xmax_; ff+=tickstep_x){
    psline(psfile__,  ps_lowercornerx_+ps_xwidth_*(ff-local_xmin_)/(local_xmax_-local_xmin_),ps_lowercornery_,\
	   ps_lowercornerx_+ps_xwidth_*(ff-local_xmin_)/(local_xmax_-local_xmin_),ps_lowercornery_+tick_length);
    
    string_txt.str("");
    string_txt<<setw(2)<<setprecision(2)<<ff;

    pstext(psfile__, ps_lowercornerx_+ps_xwidth_*(ff-local_xmin_)/(local_xmax_-local_xmin_)-5,\
	   ps_lowercornery_-12, 10.0,  (char*)string_txt.str().c_str() );

  }
  
  //add X_ticks w labels
  for(float ff=local_ymin_; ff<=local_ymax_; ff+=tickstep_y){
    psline(psfile__, ps_lowercornerx_, ps_lowercornery_+ps_ywidth_*(ff-local_ymin_)/(local_ymax_-local_ymin_),\
	   ps_lowercornerx_+tick_length, ps_lowercornery_+ps_ywidth_*(ff-local_ymin_)/(local_ymax_-local_ymin_));

    string_txt.str("");
    string_txt<<setw(2)<<setprecision(2)<<ff;
    

    pstext(psfile__, ps_lowercornerx_-13,\
	   ps_lowercornery_+ps_ywidth_*(ff-local_ymin_)/(local_ymax_-local_ymin_)-5,\
	   10.0,  (char*)string_txt.str().c_str() );


  }


  add_labels_dirty();
  return 1;

}

 
//------------------------------------------------------------------------------


int diber_output::ps_graph::draw_path(char * filename, float lwidth){

  pscolor(psfile__, rgb_, 0);


  float local_xwidth = local_xmax_-local_xmin_;
  float local_ywidth = local_ymax_-local_ymin_;
  float item_x, item_y;
  ifstream datafile;
  vector<float> path;
  float * dataxy;


  path.clear();
  datafile.open(filename);

  if (!datafile.is_open()) return 0;


  while(datafile.good()){
    datafile>>item_x>>item_y;
    item_x*=diber_model_input_data_xmax_;
    item_y*=diber_model_input_data_ymax_;
    if( contains(local_xmin_, item_x, local_xmax_) && contains(local_ymin_, item_y, local_ymax_) ){    
      path.push_back(item_x);
      path.push_back(item_y);
    }
    if(datafile.eof()) break;
  }
  datafile.close();


  dataxy = new float[path.size()];
  for(int i=0; i<path.size()/2; i++){
    dataxy[2*i]=ps_lowercornerx_+ps_xwidth_*(path[2*i]-local_xmin_)/(local_xwidth);    
    dataxy[2*i+1]=ps_lowercornery_+ps_ywidth_*(path[2*i+1]-local_ymin_)/(local_ywidth);
  }

  
 
  pspath(psfile__, dataxy, path.size()/2, lwidth, path_no_);

  path_no_++;
  delete(dataxy);
  return 1;
}

//------------------------------------------------------------------------------


int diber_output::ps_graph::draw_filledpath(char * filename, float r, float g, float b){
  
  float local_xwidth = local_xmax_-local_xmin_;
  float local_ywidth = local_ymax_-local_ymin_;
  float item_x, item_y;
  ifstream datafile;
  vector<float> path;
  float * dataxy;


  path.clear();
  datafile.open(filename);

  if (!datafile.is_open()) return 0;

  while(datafile.good()){
    datafile>>item_x>>item_y;
    path.push_back(item_x*diber_model_input_data_xmax_);
    path.push_back(item_y*diber_model_input_data_ymax_);
    if(datafile.eof()) break;
  }
  datafile.close();

  dataxy = new float[path.size()];
  float x, y;
  for(int i=0; i<path.size()/2; i++){
    x = bound(local_xmin_, path[2*i], local_xmax_);
    y = bound(local_ymin_, path[2*i+1], local_ymax_); 

    dataxy[2*i]=ps_lowercornerx_+ps_xwidth_*(x-local_xmin_)/(local_xwidth);    
    dataxy[2*i+1]=ps_lowercornery_+ps_ywidth_*(y-local_ymin_)/(local_ywidth);
  }
 
  psfilledpath(psfile__, dataxy, path.size()/2, path_no_, r, g, b);

  path_no_++;
  delete(dataxy);
  return 1;
}

//------------------------------------------------------------------------------

Diber_Error*  diber_output::ps_graph::draw_svm_classification(int predicted_class){

  Diber_Error* local_error;

  
    
  
  switch(predicted_class)
    {
    case 1:
      // ------------------------------- complexes ----------------------------------
      
      //draw_filledpath((char*)(string(getenv("DIBER")) + "/data/diber3_paths/diber3_path1_1.dat").c_str(),\
      //	      0.996, 0.898, 0.898);
      //draw_filledpath((char*)(string(getenv("DIBER")) + "/data/diber3_paths/diber3_path1_2.dat").c_str(),\
      //	      0.930, 0.809, 0.809);

      if(use_diber_){	 
	      draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path1_3.dat").c_str(),\
			      0.785, 0.625, 0.625);
	      //draw_filledpath((char*)(string(getenv("DIBER")) + "/data/diber3_paths/diber3_path1_4.dat").c_str(),\
	      //	      0.645, 0.441, 0.441);
	      draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path1_5.dat").c_str(),\
			      0.508, 0.266, 0.266);
	      draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path1_6.dat").c_str(),\
			      0.363, 0.082, 0.082);
      }else{
		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/riber3_path1_1.dat").c_str(),\
			     0.785, 0.625, 0.625);
		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/riber3_path1_2.dat").c_str(),\
			     0.508, 0.266, 0.266);
      }



      break;
    case 2:
      // ---------------------------------- DNA -------------------------------------
      //draw_filledpath((char*)(string(getenv("DIBER")) + "/data/diber3_paths/diber3_path2_1.dat").c_str(),\
      //	      0.898, 0.898, 0.996);
      //draw_filledpath((char*)(string(getenv("DIBER")) + "/data/diber3_paths/diber3_path2_2.dat").c_str(),\
      //	      0.809, 0.809, 0.914);  

	if(use_diber_){	
		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path2_3.dat").c_str(),\
			0.625, 0.625, 0.742);  
      		//draw_filledpath((char*)(string(getenv("DIBER")) + "/data/diber3_paths/diber3_path2_4.dat").c_str(),\
		//	      0.441, 0.441, 0.566);  
      		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path2_5.dat").c_str(),\
			0.266, 0.266, 0.402);  
      		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path2_6.dat").c_str(),\
		      	0.082, 0.082, 0.226);  
	
	}else{
		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/riber3_path2_1.dat").c_str(),\
			      0.625, 0.625, 0.742);
		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/riber3_path2_2.dat").c_str(),\
				0.266, 0.266, 0.402);
	}

      break;
    case 3:
      // --------------------------------- proteins ----------------------------------
      //draw_filledpath((char*)(string(getenv("DIBER")) + "/data/diber3_paths/diber3_path3_1.dat").c_str(),\
      //	      0.898, 0.996, 0.898);
      //draw_filledpath((char*)(string(getenv("DIBER")) + "/data/diber3_paths/diber3_path3_2.dat").c_str(),\
      //	      0.808, 0.933, 0.808);



      if(use_diber_){	
      		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path3_3.dat").c_str(),\
			0.625, 0.801, 0.625);
		//draw_filledpath((char*)(string(getenv("DIBER")) + "/data/diber3_paths/diber3_path3_4.dat").c_str(),\
      			//	      0.441, 0.668, 0.441);
      		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path3_5.dat").c_str(),\
		      	0.266, 0.543, 0.266);
     	 	draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path3_6.dat").c_str(),\
			0.082, 0.410, 0.082);
	}else{
		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/riber3_path3_1.dat").c_str(),\
			      0.625, 0.801, 0.625);
		draw_filledpath((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/riber3_path3_2.dat").c_str(),\
			      0.082, 0.410, 0.082);
	}


      break;
    default:
      return local_error = new Diber_Error( (char*)"SVM classification error: label not recognized.", 1);
    }


  //------------------------ optimal classification lines -----------------------
  if(use_diber_){	
  	draw_path((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path_a.dat").c_str(), 1.0);
  	draw_path((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/diber3_path_b.dat").c_str(), 1.01);
  }else{
	draw_path((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/riber3_path_a.dat").c_str(), 1.0);
  	draw_path((char*)(string(getenv("RDIBER")) + "/data/diber3_paths/riber3_path_b.dat").c_str(), 1.01);
  }

  
  // ----------------------------- ticks and labels ------------------------------
  draw_frame(1.5, 0.01, (local_ymax_-local_ymin_)/5.0);


  return local_error = new Diber_Error( (char*)"Normal termination", 0);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//
//	DIBER_OUTPUT :: PS_PROJECTION
//
//------------------------------------------------------------------------------



int diber_output::ps_projection::cartesian_to_spherical(double x, double y, double z,\
			   double * theta, double * delta, double * r){
	
  double p=sqrt(x*x + y*y);
  *r=sqrt(x*x + y*y + z*z);
  *delta=asin(z/(*r));
  if(p>0)
    if(x>0)
      *theta=asin(y/p);
    else
      *theta=M_PI-asin(y/p);
  else
    *theta=0.0;
  
  return 1;
}


//------------------------------------------------------------------------------


int diber_output::ps_projection::redistribute_values(int data_size, vector<float>* xydata, vector<float>* xyfactors, float x, float y, float val){

  int xceil = (int)bound(0., ceil(x), data_size-1.);
  int xfloor = (int)bound(0., floor(x), data_size-1.);
  int yceil = (int)bound(0., ceil(y), data_size-1.);
  int yfloor = (int)bound(0., floor(y), data_size-1.);

  
  (*xydata)[data_size*xceil + yceil]+=sqrt((x-xceil)*(x-xceil) + (y-yceil)*(y-yceil))*val;
  (*xyfactors)[data_size*xceil + yceil]+=sqrt((x-xceil)*(x-xceil) + (y-yceil)*(y-yceil));

  (*xydata)[data_size*xceil + yfloor]+=sqrt((x-xceil)*(x-xceil) + (y-yfloor)*(y-yfloor))*val;
  (*xyfactors)[data_size*xceil + yfloor]+=sqrt((x-xceil)*(x-xceil) + (y-yfloor)*(y-yfloor));
  
  (*xydata)[data_size*xfloor + yceil]+=sqrt((x-xfloor)*(x-xfloor) + (y-yceil)*(y-yceil))*val;
  (*xyfactors)[data_size*xfloor + yceil]+=sqrt((x-xfloor)*(x-xfloor) + (y-yceil)*(y-yceil));

  (*xydata)[data_size*xfloor + yfloor]+=sqrt((x-xfloor)*(x-xfloor) + (y-yfloor)*(y-yfloor))*val;
  (*xyfactors)[data_size*xfloor + yfloor]+=sqrt((x-xfloor)*(x-xfloor) + (y-yfloor)*(y-yfloor));

  return 1;
}



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



  int diber_output::ps_projection::draw_projection( diber_dataset * input_diber_dataset, int n_contour_levels ){

    

    double x_zero = ps_lowercornerx_ + 0.5*ps_width_;
    double y_zero = ps_lowercornery_ + 0.5*ps_width_;
    double angle=0.0;

    pslwidth(psfile__, .1);
    pscolor(psfile__, rgb_, 0);
   
   
    while(angle<=90.0){
      pscircle(psfile__, x_zero, y_zero, ps_width_*sin(0.5*M_PI*angle/180.0)/sqrt(2.0));
      angle+=10.0;
    }
    pslwidth(psfile__, 1.0);
    pscircle(psfile__, x_zero, y_zero, .5*ps_width_);
    pslwidth(psfile__, .1);
    float x_line[2];
    float y_line[2];
    x_line[0]=x_zero;
    y_line[0]=y_zero;
    angle = 0.0;
    
    while(angle<=360.0){
      x_line[1]=x_zero + 0.5*ps_width_*cos(M_PI*angle/180.0);
      y_line[1]=y_zero + 0.5*ps_width_*sin(M_PI*angle/180.0);
      psline(psfile__, x_line[0], y_line[0], x_line[1], y_line[1]);
      //cpgline(2, x_line, y_line);
      angle+=10.0;
    }	
    
    
    // ------------ OX AXIS -------------------
    x_line[0]=x_zero+0.5*ps_width_+10;
    y_line[0]=y_zero+0.0;
    
    x_line[1]=x_zero-0.5*ps_width_;
    y_line[1]=y_zero+0.0;
    
    
    pslwidth(psfile__, 1);
    psline(psfile__, x_line[0], y_line[0], x_line[1], y_line[1]);
    pstext(psfile__, x_line[0]-5, 2 + y_line[0], 12.0,  (char*)"X"); //was "h" 
    
    
    // ------------ OY AXIS -------------------
    x_line[0]=x_zero+0.0;
    y_line[0]=y_zero+0.5*ps_width_+10;
    
    x_line[1]=x_zero-0.0;
    y_line[1]=y_zero-0.5*ps_width_;
    
    psline(psfile__, x_line[0], y_line[0], x_line[1], y_line[1]);
    pstext(psfile__, x_line[0]+2, y_line[0] - 5, 12.0, (char*)"Y"); //was "k"	
    

    //------------------------------------------------------------------------------






    clipper::Coord_reci_frac fake_frac;
    clipper::Vec3<double> v_ortho_tmp;
    double theta_tmp, delta_tmp, r_tmp;
    float r_projection, x_projection, y_projection;
    float ave_min=9999.0, ave_max=0.0;
    pscolor(psfile__, rgb_, 2);



    vector<float> xydata_interpolated;
    vector<float> xydata_normfactors;

    float data_size = 50.0;
    double * x_data = (double*)malloc( (int)(data_size*data_size) * sizeof(double));
    double * y_data = (double*)calloc((int)(data_size*data_size), sizeof(double));
    double * z_data = (double*)calloc((int)(data_size*data_size), sizeof(double));

    xydata_interpolated.resize(int(data_size*data_size), 0.0);
    xydata_normfactors.resize(int(data_size*data_size), 0.0);
    

    for( int iq=0; iq<(input_diber_dataset->get_icodata()).size(); iq++)
      if(input_diber_dataset->get_icodata()[iq].npoints>0){
	if(input_diber_dataset->get_icodata()[iq].average_intensity>ave_max)
	  ave_max = input_diber_dataset->get_icodata()[iq].average_intensity;
	else if(input_diber_dataset->get_icodata()[iq].average_intensity<ave_min)
	  ave_min = input_diber_dataset->get_icodata()[iq].average_intensity;
      }

    for( int iq=0; iq<(input_diber_dataset->get_icodata()).size(); iq++){
      if(input_diber_dataset->get_icodata()[iq].npoints>0){

	  

	//symmetry expansion
	
	v_ortho_tmp[0] = cos(input_diber_dataset->get_icodata()[iq].theta)*\
	  cos(input_diber_dataset->get_icodata()[iq].delta);
	v_ortho_tmp[1] = sin(input_diber_dataset->get_icodata()[iq].theta)*\
	  cos(input_diber_dataset->get_icodata()[iq].delta);
	v_ortho_tmp[2] = sin(input_diber_dataset->get_icodata()[iq].delta);
      
	fake_frac = (clipper::Coord_reci_orth(v_ortho_tmp)).\
	  coord_reci_frac((input_diber_dataset->get_hkls()).cell());
	for(int jj=0; jj<(input_diber_dataset->get_hkls()).spacegroup().num_symops(); jj++ ){
	  for(clipper::ftype friedel=-1.; friedel<2.; friedel+=2.)
	    {
	      v_ortho_tmp =\
		clipper::Coord_reci_frac(friedel*fake_frac.transform((input_diber_dataset->get_hkls()).spacegroup().symop(jj))).coord_reci_orth((input_diber_dataset->get_hkls()).cell());


	      if(v_ortho_tmp[2]>=0){
		cartesian_to_spherical(v_ortho_tmp[0], v_ortho_tmp[1], v_ortho_tmp[2], &theta_tmp, &delta_tmp, &r_tmp);
		
		r_projection = sin(0.5*(.5*M_PI-delta_tmp))/sqrt(2.0);
		x_projection = r_projection*cos(theta_tmp);
		y_projection = r_projection*sin(theta_tmp);
		
		

		redistribute_values(int(data_size), &xydata_interpolated, &xydata_normfactors,\
				    (data_size-1.)*(x_projection+0.5), (data_size-1.)*(y_projection+0.5),\
				    input_diber_dataset->get_icodata()[iq].average_intensity);
		
		
	      }
	    }
	}
      }
    }
    pscolor(psfile__, rgb_, 0);

    //here, an interpolation of the spherical projection on a rectangular 
    //grid is finalized (xydata_interpolated/xydata_normfactors)
 
    for(int i=0; i<data_size; i++)
      for(int j=0; j<data_size; j++){
	
	x_data[(int)data_size*i+j] = (ps_width_)*((float)(i)/(data_size-1.)-0.5) + x_zero;
	y_data[(int)data_size*i+j] = (ps_width_)*((float)(j)/(data_size-1.)-0.5) + y_zero;

	
	//mask data with the outer projection circle...
	if( (xydata_normfactors[int(data_size)*i+j]>0.0) && (pow((float)(i)/(data_size-1.0)-0.5, 2.0) +\
							     pow((float)(j)/(data_size-1.0)-0.5, 2.0) < 0.25) ) 
	  z_data[(int)data_size*i+j]=xydata_interpolated[int(data_size)*i + j]/xydata_normfactors[int(data_size)*i + j];
	else{
	  z_data[(int)data_size*i+j]=0.0;
	 
	}


      }
    
    float delta = (ave_max-ave_min)/float(n_contour_levels);
    for(float level=ave_min+delta; level<ave_max; level+=delta)
      contour_tracer(psfile__, x_data, y_data, z_data, data_size, level);


    return 1;
    
    }



//------------------------------------------------------------------------------




int diber_output::ps_projection::contour_tracer(FILE * psfile, double *x_data, double *y_data, double *z_data, float data_size, float level){


  
  Csite *site;

  site = new(Csite);
  site->data = NULL;
  site->reg = NULL;
  site->triangle = NULL;
  site->xcp = NULL;
  site->ycp = NULL;
  site->x = NULL;
  site->y = NULL;
  site->z = NULL;
  long ijmax = (long)(data_size * data_size);
  long nreg = (long)(data_size * data_size + data_size + 1);
  
  
  site->x = x_data;
  site->y = y_data;
  site->z = z_data;
  site->imax = (long)(data_size);
  site->jmax = (long)(data_size);
  site->data = new Cdata[nreg];
  site->triangle = new short[ijmax];

  for (int i = 0; i < ijmax; i++) site->triangle[i] = 0;
  site->reg = NULL;
  
  
  site->zlevel[0]=level;
  site->zlevel[1]=level;
  
  long n;
  long nparts = 0;
  long ntotal = 0;
  long nchunk=0;
  long nparts2 = 0;
  long ntotal2 = 0;
  long *nseg0;
  int iseg;


  /* first pass, checks number of segments and parts*/
  site->n = site->count = 0;
  data_init (site, 0, nchunk);
  
  for (;;)
    {
      n = curve_tracer (site, 0);
      //cout<<n<<endl;
      if (!n)
	break;
      if (n > 0)
        {
	  nparts++;
	  ntotal += n;
        }
      else
        {
	  ntotal -= n;
        }
    }
  
  
  double * xp0 = new double[ntotal];
  double * yp0 = new double[ntotal];
  nseg0 = new long[nparts];
  
  /* second pass */
  site->xcp = xp0;
  site->ycp = yp0;
  iseg = 0;
  for (;;iseg++)
    {
      n = curve_tracer (site, 1);
      if (ntotal2 + n > ntotal)
        {
	  fprintf(stderr, "DIBER contour_tracer error: ntotal2, pass 2 exceeds ntotal, pass 1");
          return 0;
        }
      if (n == 0)
	break;
      if (n > 0)
        {
	  /* could add array bounds checking */
	  nseg0[iseg] = n;
	  site->xcp += n;
	  site->ycp += n;
	  ntotal2 += n;
	  nparts2++;
        }
      else
        {
	  fprintf(stderr,  "DIBER contour_tracer error: Negative n from curve_tracer in pass 2");
	  return 0;
        }
    }
  float *path;
  int k=0;
  for (int i = 0; i < nparts; i++){
    path = new float[2*nseg0[i]];
    for (int j = 0; j < nseg0[i]; j++){
      path[2*j]=xp0[k];
      path[2*j+1]=yp0[k];
      k++;
    }
    pspath(psfile, path, nseg0[i], 0.5, path_no_++);
    delete(path);
  }


  
  delete(site->data);
  delete(site->triangle);
  delete(xp0);
  delete(yp0);
  delete(site);
  
  return 1;

}


//------------------------------------------------------------------------------


int diber_output::ps_projection::mark_phaser_solutions ( vector<float>* euler_angles, float size, float lwidth ){



  float r_projection;
  float	x_projection;
  float	y_projection;
  float x_zero = ps_lowercornerx_ + 0.5*ps_width_;
  float y_zero = ps_lowercornery_ + 0.5*ps_width_;

  pscolor(psfile__, rgb_, 0);
  pslwidth(psfile__, lwidth);


  for(int i=0; i<euler_angles->size()/3; i++){
    r_projection = sin(0.5*(*euler_angles)[3*i+1])/sqrt(2.0);
    x_projection = r_projection*cos((*euler_angles)[3*i]);
    y_projection = r_projection*sin((*euler_angles)[3*i]);

    psx(psfile__, x_zero+ps_width_*x_projection, y_zero+ps_width_*y_projection, size, lwidth);
  

  }

  return 1;
}



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//
//	DIBER_OUTPUT :: SUPPORT VECTOR MACHINES
//
//------------------------------------------------------------------------------

vector<string>* diber_output::prepare_plain_english_output( int predict_label, double * prob_estimates, const float improbable_cutoff=0.01)
{
  vector<string>* output_strings;
  output_strings = new vector<string>;
  
  output_strings->resize(0);
  stringstream string_tmp;
  string_tmp.str("");

  string_tmp<<diber_dataset_input_->get_program_mode()<<" says:";

  (*output_strings).push_back(string_tmp.str());
  (*output_strings).push_back("");
  string_tmp.str("");

  string predicted_name, other1_name, other2_name;
  int other1_label, other2_label;
  switch(predict_label)
    {
    case 1:
      predicted_name = "a complex";
      other1_name = diber_dataset_input_->get_nacid_name();
      other1_name+=" only";
      other2_name = "only protein";

      other1_label=1;
      other2_label=2;
      break;
    case 2:
      predicted_name = diber_dataset_input_->get_nacid_name();
      predicted_name+=" only";
      other1_name = "a complex";
      other2_name = "only protein";

      other1_label=0;
      other2_label=2;
      break;
    case 3:
      predicted_name = "only protein";
      other1_name = "a complex";
      other2_name = diber_dataset_input_->get_nacid_name();
      other2_name+=" only";

      other1_label=0;
      other2_label=1;
      break;
    }
  

  //1st step
  if(prob_estimates[predict_label-1]>0.99){
    (*output_strings).push_back( string("Your crystal almost certainly contains ") + predicted_name + string("."));
    return output_strings;
  }
  else if(prob_estimates[predict_label-1]>0.9)
    {
      (*output_strings).push_back( string("Your crystal is very likely to contain ") + predicted_name + string(".") );
    }
  else if(prob_estimates[predict_label-1]>0.8)
    {
      (*output_strings).push_back( string("Your crystal is likely to contain ") + predicted_name + string(".") );
    }
  else
    {
      (*output_strings).push_back( "The result is not clear, but your crystal probably" );
      (*output_strings).push_back(string("contains ") + predicted_name + string("."));
    }
  
  
  //2nd step
  if ( (prob_estimates[other2_label]<improbable_cutoff ) && (prob_estimates[other1_label]<improbable_cutoff ) )
    {
      (*output_strings).push_back( string("It almost certainly does not contain ") + other1_name); 
      (*output_strings).push_back( string("nor ") + other2_name + string(".")); 
      
    }
  else if ( (prob_estimates[other2_label]<improbable_cutoff ) )
    {
      string_tmp<<"It may contain "<<other1_name<<" with "<<\
	setiosflags(ios::fixed)<<setprecision(0)<<prob_estimates[other1_label]*100.0<<\
	"\% probability,";
      (*output_strings).push_back(string_tmp.str());
      (*output_strings).push_back( string("but almost certainly does not contain ") + other2_name + string(".")); 
      
    }
  else if ( (prob_estimates[other1_label]<improbable_cutoff ) )
    {
      string_tmp<<"It may contain "<<other2_name<<" with "<<\
	setiosflags(ios::fixed)<<setprecision(0)<<prob_estimates[other2_label]*100.0<<\
	"\% probability,";
      (*output_strings).push_back(string_tmp.str());
      (*output_strings).push_back( string("but almost certainly does not contain ") + other1_name + string(".")); 
    }
  else
    {
      string_tmp<<"It may contain "<<other1_name<<" with "<<\
	setiosflags(ios::fixed)<<setprecision(0)<<prob_estimates[other1_label]*100.0<<\
	"\% probability,";
      (*output_strings).push_back(string_tmp.str());
      string_tmp.str("");
      string_tmp<<"or "<<other2_name<<" with "<<\
	setiosflags(ios::fixed)<<setprecision(0)<<prob_estimates[other2_label]*100.0<<\
	"\% probability.";
      (*output_strings).push_back(string_tmp.str());
    }


  return output_strings;
}




//------------------------------------------------------------------------------
//
//	DIBER_OUTPUT :: SUPPORT VECTOR MACHINES WITH PHASER SCORE
//
//------------------------------------------------------------------------------


Diber_Error* diber_output::play_with_svm_phaser_enhanced(FILE * psfile){

  Diber_Error* local_error;
  ps_graph * graph;
  stringstream string_txt;
  string_txt.setf(ios::fixed);
  string_txt.setf(ios::showpoint);
  string_txt.precision(2);
  svm_node* x;

  vector<string>* plain_english_output_strings;


  //init svm model
  svm_model* model;
  int predict_label;
  double *prob_estimates=NULL;

  printf ("\n ------------------------------------------------------------------------------\n"
	  "  %s Starting Support Vector Machine\n\n", diber_dataset_input_->get_program_mode() );

  if(!diber_dataset_input_->use_diber()){
    if( !(model=svm_load_model((string(getenv("RDIBER")) +\
				"/data/riber_model.dat").c_str())) )
      return local_error_ =\
	new Diber_Error( (char*)"ERROR: Unable to open a SVM model - check your installation! ", 1);
    //cout<<"RIBER model..."<<endl;
  }else if(clipper::Util::is_nan( diber_dataset_input_->get_phaser_max_llg() )){
    if( !(model=svm_load_model((string(getenv("RDIBER")) +\
				"/data/diber_model.dat").c_str())) )
      return local_error_ =\
	new Diber_Error( (char*)"ERROR: Unable to open a SVM model - check your installation! ", 1);
  }else
    if( !(model=svm_load_model((string(getenv("RDIBER")) + "/data/diberphaser_model.dat").c_str())) )
      return local_error =\
	new Diber_Error( (char*)"ERROR: Unable to open a SVM model - check your installation! ", 1);
  

 
  printf("    The strongest average E^2   %10.4f\n", diber_dataset_input_->get_max_average());
  if(!clipper::Util::is_nan( diber_dataset_input_->get_phaser_max_llg() ))
    printf("    Phaser LLG                  %10.4f\n", diber_dataset_input_->get_phaser_max_llg());
  printf("    1/cuberoot(UC volume)       %10.4f\n", diber_dataset_input_->get_oneovercrVol());




  //preapre input for a model

  if(clipper::Util::is_nan( diber_dataset_input_->get_phaser_max_llg() )){
    x = new svm_node[3];
    
    x[0].value = diber_dataset_input_->get_oneovercrVol()/diber_model_input_data_xmax_;
    x[1].value = diber_dataset_input_->get_max_average()/diber_model_input_data_ymax_;

    x[0].index = 0;
    x[1].index = 1;
    x[2].index = -1;
  }else{
    x = new svm_node[4];

    x[0].value = diber_dataset_input_->get_oneovercrVol()/diber_model_input_data_xmax_;
    x[1].value = diber_dataset_input_->get_max_average()/diber_model_input_data_ymax_;
    x[2].value = (diber_dataset_input_->get_phaser_max_llg()+phaser_model_input_data_yshift_)/phaser_model_input_data_ymax_;
    
    x[0].index = 0;
    x[1].index = 1;
    x[2].index = 2;
    x[3].index = -1;
  }



  int nr_class=svm_get_nr_class(model);

  prob_estimates = new double[nr_class];


  predict_label = (int)(svm_predict_probability(model, x, prob_estimates));

  diber_dataset_input_->set_svm_prob_estimates(prob_estimates);

  //---------------------------- here projections and SVM pdfs are drawn if required -----------------------------

  if(psfile!=NULL){
    if(clipper::Util::is_nan( diber_dataset_input_->get_phaser_max_llg() )){
      graph = new ps_graph(psfile, 0.0, diber_model_input_data_xmax_, 0.0, diber_model_input_data_ymax_, 150, 310, 300, 200, diber_dataset_input_->use_diber());
      if( (local_error_ = graph->draw_svm_classification(predict_label))->Number()) { delete(graph); return local_error_; } 
      graph->draw_marker(diber_dataset_input_->get_oneovercrVol(), diber_dataset_input_->get_max_average(), 5.0, 1.5);
      delete(graph);
      
      graph =\
	new ps_graph(psfile, 0.0, diber_model_input_data_xmax_, 0.0,\
		     bound(3, diber_dataset_input_->get_max_average()+1, diber_model_input_data_ymax_/1.0), 150, 110, 300, 150, diber_dataset_input_->use_diber());
      
      if( (local_error_ = graph->draw_svm_classification(predict_label))->Number()) { delete(graph); return local_error_; }
      graph->draw_marker(diber_dataset_input_->get_oneovercrVol(), diber_dataset_input_->get_max_average(), 5.0, 1.5);
      delete(graph);
  
      ps_projection * projection = new ps_projection(psfile, 300, 530, 200);
      projection->draw_projection(diber_dataset_input_, 10);

    }else{
      //---- Diber will prepare ONLY spherical projection, SVM classification
      //---- graph doesn't fit plane...
      ps_projection * projection = new ps_projection(psfile, 200, 350, 200);
      projection->draw_projection(diber_dataset_input_, 10);
      projection->mark_phaser_solutions( &diber_dataset_input_->get_phaser_solutions(), 5.0, 1.5 );
    }
  }



  printf( "\n<!--SUMMARY_BEGIN-->" );
  printf ("\n ------------------------------------------------------------------------------\n"
	  "  %s Output\n\n", diber_dataset_input_->get_program_mode() );

  

  plain_english_output_strings=prepare_plain_english_output(predict_label, prob_estimates);
  

  for(int line_no=0; line_no<plain_english_output_strings->size(); line_no++){
    cout<<"    "<<(*plain_english_output_strings)[line_no]<<endl;
    plain_english_output_one_string_+=(*plain_english_output_strings)[line_no]+string("\n");
  }

  if(psfile!=NULL)
    for(int line_no=0; line_no<plain_english_output_strings->size(); line_no++)
      pstext( psfile_, 70, 670-line_no*12, 10.0, (char*)((*plain_english_output_strings)[line_no]).c_str() );

  printf( "\n<!--SUMMARY_END-->\n\n" );



  printf("  Predicted probabilities, that your crystal contains: \n\n");
  
 
  printf("    complex  ");
  draw_horizontal_bar(prob_estimates[0]);
  printf("    %s only ", diber_dataset_input_->get_nacid_name());
  draw_horizontal_bar(prob_estimates[1]);
  printf("    protein  ");
  draw_horizontal_bar(prob_estimates[2]);
  printf("\n\n\n");
  

  delete(prob_estimates);
  delete(x);
  svm_destroy_model(model);

  return local_error = new Diber_Error( (char*)"Normal termination", 0);
}
