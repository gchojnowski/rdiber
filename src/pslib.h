//******************************************************************************
//
//                PostScriptLib 1.0.1 (07.05.2007)
//                    based on PostScript fortran subroutines
//                    written by Roman Laskowski (PROCHECK)
//                    and modified by Alexei Vagin (MOLREP)
//                                   
//******************************************************************************

#include <stdio.h>
#include <stdlib.h>



// -----------------------------------------------------------------------------
//
//  assign rgb colors table
//
// -----------------------------------------------------------------------------

float * set_rgb(int * maxcolor);



// -----------------------------------------------------------------------------
//
//   psopen -  Create a new PostScript file
//
// -----------------------------------------------------------------------------

FILE * psopen(char * fname, int bboxx1, int bboxx2, int bboxy1, int bboxy2, int * maxcolor,\
			float * rgb, int incol, int bakcol, int ierr, int total_pages, int  current_page);
//rgb - 3 x maxcolor matrix




// -----------------------------------------------------------------------------
//
//   psresource_new -  Write resource to a PostScript file
//
// -----------------------------------------------------------------------------

int psresource_new(FILE * psfile);




// ----------------------------------------------------------------------------- 			 
//						  							 
//    psnewpage  -  Write new page lines to a PostScript file				 
// 						 
// -----------------------------------------------------------------------------
			 
int psnewpage(FILE * psfile, int bboxx1, int bboxx2, int bboxy1, int bboxy2, int * maxcolor,\
			float * rgb, int incol, int bakcol, int ierr, int total_pages, int  * current_page);




// -----------------------------------------------------------------------------
//
//    psclose  -  Write final lines to a PostScript file and close
//
// -----------------------------------------------------------------------------

int psclose(FILE * psfile, int bboxx1,int bboxx2,int bboxy1,int bboxy2);




// -----------------------------------------------------------------------------
//
//    psbbox -  Write out bounded box to a PostScript file
//
// -----------------------------------------------------------------------------

int psbbox(FILE * psfile, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);




// -----------------------------------------------------------------------------
//
//    psubox -  Write out unbounded box to a PostScript file
//
// -----------------------------------------------------------------------------

int psubox(FILE * psfile, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);




// -----------------------------------------------------------------------------
//
//   psrgbcolorb  -  Write background colour level with rgb values
//
//------------------------------------------------------------------------------

int psrgbcolorb(FILE * psfile, float r, float g, float b);




// -----------------------------------------------------------------------------
//
//   pscolorb  -  Write background colour level basing on color number on rgb table
//
//------------------------------------------------------------------------------

int pscolorb(FILE * psfile, float * rgb, int color);




// -----------------------------------------------------------------------------
//
//  psrgbcolor  -  Write colour level
//
//------------------------------------------------------------------------------

int psrgbcolor(FILE * psfile, float r, float g, float b);




// -----------------------------------------------------------------------------
//
//  pscolor  -  Write colour level basing on color number on rgb table
//
//------------------------------------------------------------------------------
	
int pscolor(FILE * psfile, float * rgb, int color);




// -----------------------------------------------------------------------------
//
//  psdisk  -  Write out a full circle (disk) to a PostScript file
//
//------------------------------------------------------------------------------

int psdisk(FILE * psfile, float x, float y, float radius);

// -----------------------------------------------------------------------------
//
//  psx  -  Write out an X to a PostScript file
//
//------------------------------------------------------------------------------

int psx(FILE * psfile, float x, float y, float size, float lwidth);

// -----------------------------------------------------------------------------
//
//  pscircle  -  Write out a open circle to a PostScript file
//
//------------------------------------------------------------------------------

int pscircle(FILE * psfile, float x, float y, float radius);




// -----------------------------------------------------------------------------
//
//  psctext  -  Write out centred text to a PostScript file
//
//------------------------------------------------------------------------------

int psctext(FILE * psfile, float x, float y, float size, char * text);




// -----------------------------------------------------------------------------
// 
// psline  -  Write line out to a PostScript file
//
//------------------------------------------------------------------------------

int psline(FILE * psfile, float x1, float y1, float x2, float y2);




// -----------------------------------------------------------------------------
//
//  pslwidth  -  Write line-width out to a PostScript file
//
//------------------------------------------------------------------------------

int pslwidth(FILE * psfile, float lwidth);




// -----------------------------------------------------------------------------
//
//  psmarker  -  Write point-marker to a PostScript file
//  1 - box
//  2 - open circle
//  3 - full circle
//  4 - open triangle
//
//------------------------------------------------------------------------------

int psmarker(FILE * psfile, float x, float y, int marker, float widthx, float widthy);




// -----------------------------------------------------------------------------
//
//  psmarkers  -  Write n point-markers to a PostScript file with coordinates (x[], y[])
//	              markers in PS file are in groups of 10 per line to minimize disk space usage 
//  1 - box
//  2 - open circle
//  3 - full circle
//  4 - open triangle
//
//------------------------------------------------------------------------------

int psmarkers(FILE * psfile, float * x, float * y, int n, int marker, float widthx, float widthy);




// -----------------------------------------------------------------------------
//
//  psrotctext  -  Write out text centred and rotated through 90
//                        degrees to PostScript file
//
//------------------------------------------------------------------------------

int psrotctext(FILE * psfile, float x, float y, float size, char * text);




// -----------------------------------------------------------------------------
//
//  psrottext -  Write out text rotated through 90 degrees to
//                        PostScript file
//
//------------------------------------------------------------------------------

int psrottext(FILE * psfile, float x, float y, float size, char * text);




// -----------------------------------------------------------------------------
//
//  psdash  -  Switch dashed lines on/off
//
//------------------------------------------------------------------------------

int psdash(FILE * psfile, int onoff);




// -----------------------------------------------------------------------------
//
//  psshade  -  Write shading level out to a PostScript file
//
//------------------------------------------------------------------------------

int psshade(FILE * psfile, float shade, int colour, float * rgb,  int mxcolr, int incol);




// -----------------------------------------------------------------------------
//
//  pstext  -  Write out text to a PostScript file
//
//------------------------------------------------------------------------------

int pstext(FILE * psfile, float x, float y, float size, char * text);




// -----------------------------------------------------------------------------
//
//  psbtext  -  Write out text to a PostScript file
//
//------------------------------------------------------------------------------

int psbtext(FILE * psfile, float x, float y, float size, char * text);




// -----------------------------------------------------------------------------
//
//  pssymbol  -  Write out symbol text to a PostScript file
//
// -----------------------------------------------------------------------------

int pssymbol(FILE * psfile, float x, float y, float size, char * text);








// -----------------------------------------------------------------------------
//
//  pspath  -  Create path based on a given list of points
//
// -----------------------------------------------------------------------------




int pspath(FILE * psfile, float * points, int n, float lwidth, int path_no);



int psfilledpath(FILE * psfile, float * points, int n, int path_no, float r, float g, float b);




































