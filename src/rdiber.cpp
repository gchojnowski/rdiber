/*    rdiper.cpp: Input handling and the program flow managment   */

//    Copyright (C) 2009 Grzegorz Chojnowski
//
//    This file is part of DIBER.
//
//    DIBER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    DIBER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with DIBER.  If not, see <http://www.gnu.org/licenses/>.



#include "rdiber_funcs.h"
#include "clipper/ccp4/ccp4_utils.h" 


using namespace CCP4;





int main(int n_arg, char **args){


  int verbose;
  stringstream mtzin_filename; 
  stringstream labin_FPlabel;
  stringstream labin_SIGFPlabel;
  stringstream psout_filename;
  stringstream phaser_root;
  stringstream program_mode; 

  labin_FPlabel.str("");
  labin_SIGFPlabel.str("");
  int phaser_enhanced_mode=0;
  int use_diber=-3;


  // input parser parameters
  int           ntok=0;
  char          line[201], *key;
  CCP4PARSERTOKEN * token = NULL;
  CCP4PARSERARRAY * parser;
  
  
  printf("\n\n ------------------------------------------------------------------------------\n");
  printf("  RIBER/DIBER version 5.1/20.11.2011                               \n\n" );
  
  printf(" If you find this tool useful please cite:\n");
  printf(" \"DIBER: protein, DNA, or both?\"\n");
  printf(" G. Chojnowski and M. Bochtler Acta Crystallographica D66, 643-653, (2010)\n\n");
 
  //check if the environment variable DIBER is defined
  if(getenv("RDIBER")==NULL){
    ccperror(1, "ERROR: Please set environmental variable RDIBER");
  }

  phaser_root.str("");
  labin_FPlabel.str("");
  labin_SIGFPlabel.str("");


  CCP4CommandInput cmd_args( n_arg, args, false );

  int iarg = 0;
  while ( ++iarg < cmd_args.size() )
    if ( cmd_args[iarg] == "-mtzin" ) {
	    if ( (++iarg < cmd_args.size())&&(cmd_args[iarg][0]!='-') ) mtzin_filename<<cmd_args[iarg];
        else iarg--;
    } else if ( cmd_args[iarg] == "-f" ) {
	    if ( (++iarg < cmd_args.size())&&(cmd_args[iarg][0]!='-') ) labin_FPlabel<<cmd_args[iarg];
        iarg--;
    } else if ( cmd_args[iarg] == "-sigf" ) { 
	    if ( (++iarg < cmd_args.size())&&(cmd_args[iarg][0]!='-') ) labin_SIGFPlabel<<cmd_args[iarg];
        else iarg--;
    } else if ( cmd_args[iarg] == "-phaser" ) {
	    if ( (++iarg < cmd_args.size())&&(cmd_args[iarg][0]!='-') ) {
	    	phaser_root<<cmd_args[iarg]; 
	        phaser_enhanced_mode=1;
	    } else  {
	        phaser_enhanced_mode=1;
            iarg--;
        }	
    } else if ( cmd_args[iarg] == "-mode" ){
	    if ( (++iarg < cmd_args.size())&&(cmd_args[iarg][0]!='-') ){	
    		if ( cmd_args[iarg]=="diber" ){
    			program_mode<<"DIBER";
    			use_diber=1;
    		} else if( cmd_args[iarg]=="riber" ){
    			program_mode<<"RIBER";
    			use_diber=0;
    		}
    	} else iarg--;
    } else if ( cmd_args[iarg] == "-psout" ) {
	    if ( (++iarg < cmd_args.size())&&(cmd_args[iarg][0]!='-') ) 
	    	psout_filename<<cmd_args[iarg]; 
        else iarg--;
    }

    if (!use_diber) phaser_enhanced_mode=0;


  if ( cmd_args.size() <= 1 ) {
	cout<<"Usage: rdiber -mtzin <filename>  -f <F column label> -sigf <sigF column label> -mode <diber|riber> -phaser <output filename> -psout <output filename>"<<endl<<flush;
	exit(1);
  }




  if(mtzin_filename.str().length()==0){
	cout<<"ERROR: No input file given."<<endl<<flush;
	exit(1);
  }




  if(use_diber<0){
	cout<<"ERROR: Program MODE not specified."<<endl<<flush;
	exit(1);
  }


  if( (labin_FPlabel.str().length()==0)||(labin_SIGFPlabel.str().length()==0)){
	cout<<"ERROR: Error in label assignments in LABIN."<<endl<<flush;
	exit(1);
  }
  

  int error_number;
  printf("\n ------------------------------------------------------------------------------\n"
	"  %s Input cards\n\n", program_mode.str().c_str() );
  cout<<"    MTZIN   "<<mtzin_filename.str()<<endl;
  cout<<"    FP      "<<labin_FPlabel.str()<<endl;
  cout<<"    SIGFP   "<<labin_SIGFPlabel.str()<<endl;
  if(psout_filename.str().length()>0)
    cout<<"    PSOUT   "<<psout_filename.str()<<endl;
  if(phaser_enhanced_mode){
    cout<<"    PHASER  ";
    if(phaser_root.str().length()>0)
      cout<<phaser_root.str()<<endl;
    else
      cout<<endl;
  }
  cout<<"    MODE    "<<program_mode.str()<<endl;



  if(phaser_enhanced_mode)
    cout<<"\n    DIBER will use PHASER fast rotation function to enhance prediction reliability."<<endl;
  
	  program_mode.str("");
  if(phaser_enhanced_mode and phaser_root.str().length()==0)
    cout<<"\n    WARNING: No root name for PHASER output files given. DIBER will create one from MTZIN base."<<endl;




  
  Diber_Error * diberr;
  diber_dataset * my_dataset;
  diber_scores * my_scores;
  diber_output * my_output;



  my_dataset = new diber_dataset(use_diber);
  my_scores = new diber_scores( my_dataset );
  my_output = new diber_output( my_dataset );
  

  
  diberr =\
    my_dataset->read_mtzfile( (char *)mtzin_filename.str().c_str(), (char *)labin_FPlabel.str().c_str(), (char *)labin_SIGFPlabel.str().c_str());
 

  if(!diberr->Number())
    diberr = my_scores->calculate_all_averages();
  

  if(!diberr->Number() and phaser_enhanced_mode)
    diberr = my_scores->run_phaser( (char *)mtzin_filename.str().c_str(), (char *)labin_FPlabel.str().c_str(), phaser_root.str() );
  

  if(!diberr->Number())
    diberr = my_output->create_output( (char*)psout_filename.str().c_str() );

  
  delete(my_output);
  delete(my_scores);
  delete(my_dataset);
  


  if (diberr->Number())
	cout<<diberr->Message()<<endl<<flush;


  return 0;
  
}
