//******************************************************************************
//
//  Clipper anisotropy correction
//
//  sfscale.cpp: structure factor anisotropic scaling implementation
//  Copyright (C) 2000-2006 Kevin Cowtan and University of York
//
//  Code restricting resolution for calculating the anisotropic scaling factors
//  added by Grzegorz Chojnowski 2009
//
//******************************************************************************



#include <clipper/clipper.h>





inline bool contains( float& min, float& val, float& max  ) { return ( val >= min && val <= max ); }



/* sfscale.cpp: structure factor anisotropic scaling implementation */
  //C Copyright (C) 2000-2006 Kevin Cowtan and University of York
int clipper_anisotropy_correction( clipper::HKL_data< clipper::datatypes::F_sigF<float> >& fs_input, float invresolsq_min, float invresolsq_max );
