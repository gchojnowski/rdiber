/*    rdiper_funcs.cpp: main modules header file     */

//    Copyright (C) 2009 Grzegorz Chojnowski
//
//    This file is part of DIBER.
//
//    DIBER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    DIBER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with DIBER.  If not, see <http://www.gnu.org/licenses/>.



//clipper
#include <clipper/clipper.h>
//#include <clipper/clipper-contrib.h>
#include <algorithm>
#include <clipper/clipper-ccp4.h>
#include <clipper/core/clipper_util.h>

//ccp4
#include <clipper/ccp4/ccp4_mtz_io.h>
#include <clipper/core/clipper_types.h>
#include <ccp4_parser.h>
#include <ccp4_general.h>
#include <ccp4_program.h>
#include <csymlib.h>
#include <cmtzlib.h>

//streams&co
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>



//std&co
#include <vector>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>


//local libs
#include "pslib.h"
#include "cntr.h"
#include "rdiber_svm.h"
#include "rdiber_aniso.h"

using namespace std;






vector<string> split_into_words(const string& s, const string& delim, const bool keep_empty);
/* 
{
    vector<string> result;
    if (delim.empty()) {
        result.push_back(s);
        return result;
    }
    string::const_iterator substart = s.begin(), subend;
    while (true) {
        subend = search(substart, s.end(), delim.begin(), delim.end());
        string temp(substart, subend);
        if (keep_empty || !temp.empty()) {
            result.push_back(temp);
        }
        if (subend == s.end()) {
            break;
        }
        substart = subend + delim.size();
    }
    return result;
}
*/



//------------------------- ICOSAHEDRAL COVERING POINT -------------------------


struct delta_theta{
  double delta;
  double theta;
  double average_intensity;
  double npoints;
};


//--------------------------- ERROR HANDLER FOR DIBER --------------------------


class Diber_Error
{
 private:
  int error_number_;
  char* diber_message_;
 public:
  Diber_Error();
  Diber_Error( char* message, int en ) { diber_message_ = message; error_number_ = en; };
  int Report() { cout<<diber_message_<<endl; };
  char * Message() { return diber_message_; };
  int Number() { return error_number_; };

};


//--------------------- SIMPLE DATA-CONTAINER WITH MTZIO -----------------------


class diber_dataset
{
 private:
  
  inline bool contains( float& min, float& val, float& max  )\
    const { return ( val >= min && val <= max ); }

  vector<float> aniso_eigenvector_;

  clipper::HKL_data< clipper::datatypes::F_sigF<float> >* input_hkldata_fsigf_;
  clipper::HKL_info* input_hkls_;
  vector<delta_theta> icodata_;
  Diber_Error* local_error_;

  string labin_;

  std::vector<double> prob_estimates_;

  

  // Outer and inner radii of the shell omitted in calculations
  // of the scaling factors in anisotropy correction and normalization
  // procedures. These correspond to resolution 3.371\AA and 3.429\AA.
  // respectively.

  // static const float shell_omit_invresolsq_max_ = 0.08800;
  // static const float shell_omit_invresolsq_min_ = 0.08505;

  // Riber correction; roughly same shell width (+/-0.03\AA), but centering is at 3.3\AA
  
  /*static const*/ float shell_omit_invresolsq_min_;// = 0.09018;
  /*static const*/ float shell_omit_invresolsq_max_;// = 0.09351;


  const char* rna_or_dna_;
  const char* program_mode_;
  int use_diber_;

  float oneovercrtVol_;
  float max_average_;
  float max_average_L_;
  char* hklin_fname_;
  float phaser_max_llg_;

  char* FP_label_;
  char* SIGFP_label_;

  vector<float> phaser_solutions_euler_angles_;
  vector<clipper::String> mtz_file_info_;

 public:
  int set_svm_prob_estimates(double *prob_estimates);
  std::vector<double> get_svm_prob_estimates();
  float get_oneovercrVol() { return oneovercrtVol_; };
  void set_max_average(float mave, float mL) { max_average_=mave; max_average_L_ = mL; };
  float get_max_average() { return max_average_; };
  float get_max_average_L() { return max_average_L_; };


  const char * get_program_mode() { return program_mode_; }
  const char* get_nacid_name() { return rna_or_dna_; }
  int use_diber() { return use_diber_; }

  char * get_hklin_fname() { return hklin_fname_; };
  char * get_labin() { return (char*)labin_.c_str(); };

  float get_phaser_max_llg() { return phaser_max_llg_; };
  float set_phaser_max_llg(float max_llg) { phaser_max_llg_=max_llg; };


  vector<float> get_aniso_eigenvector() { return aniso_eigenvector_; };

  float get_shell_omit_invresolsq_min() { return shell_omit_invresolsq_min_; };
  float get_shell_omit_invresolsq_max() { return shell_omit_invresolsq_max_; };

  clipper::HKL_data< clipper::datatypes::F_sigF<float> >& get_hkldata_fsigf() const { return *input_hkldata_fsigf_; };
  clipper::HKL_info& get_hkls() const { return *input_hkls_; };
  vector<delta_theta>& get_icodata() { return icodata_; };

  vector<float>& get_phaser_solutions() { return phaser_solutions_euler_angles_; };

  Diber_Error* read_mtzfile(char * hklin_fname, char * FP_label, char * SIGFP_label);

  Diber_Error* list_mtzfile_columns(char * hklin_fname, vector<clipper::String> * column_labels);
  
  //init variables and read input mtzfile
  diber_dataset() { icodata_.clear(); max_average_=-3.0; max_average_L_=0.0; phaser_max_llg_=clipper::Util::nan(); phaser_solutions_euler_angles_.clear(); input_hkls_ = new clipper::HKL_info(); input_hkldata_fsigf_ =  new clipper::HKL_data< clipper::datatypes::F_sigF<float> >; shell_omit_invresolsq_max_=0.08800; shell_omit_invresolsq_min_=0.08505;};

  
  diber_dataset(int use_diber) { use_diber_=use_diber; icodata_.clear(); max_average_=-3.0; max_average_L_=0.0; phaser_max_llg_=clipper::Util::nan(); phaser_solutions_euler_angles_.clear(); input_hkls_ = new clipper::HKL_info(); input_hkldata_fsigf_ =  new clipper::HKL_data< clipper::datatypes::F_sigF<float> >; if(use_diber){program_mode_="DIBER"; rna_or_dna_="DNA"; shell_omit_invresolsq_max_=0.08800; shell_omit_invresolsq_min_=0.08505;}else{program_mode_="RIBER"; rna_or_dna_="RNA"; shell_omit_invresolsq_max_=0.09351; shell_omit_invresolsq_min_=0.09018;}};



  ~diber_dataset() { delete(input_hkls_); delete(input_hkldata_fsigf_); icodata_.clear(); phaser_solutions_euler_angles_.clear(); };
};




//*******************************************************************************
//**************************** CALCULATIONS *************************************
//*******************************************************************************

class diber_scores
{
 private:
  //XXX
  float _shell_resolution;
  float _hwhm_R;
  float _hwhm_zs;

  inline bool contains( float& min, float& val, float& max  )\
	      const { return ( val >= min && val <= max ); }

  
  Diber_Error* local_error_;


  //--------------------- peripherals -----------------
  class progress_bar{
  private:
    int total_width;
    double chunk_size;
    int chunk_no;

    
  public:
    progress_bar(int width);
    ~progress_bar(void);
    int progress_bar_increase(double total_progress);

   
  };



  //---------------------     data     -----------------


  diber_dataset* input_diber_dataset;
  clipper::HKL_data< clipper::datatypes::E_sigE<float> >* hkldata_esige_originalspg;
  clipper::HKL_data< clipper::datatypes::E_sigE<float> >* hkldata_esige_p1;

  clipper::HKL_info* hkls_p1;



  //---------------------    tools     -----------------

  Diber_Error* calculate_Evalues();
  Diber_Error* expand_to_P1();
  Diber_Error* read_icosahedral_covering_data();

  int calculate_single_average(double theta_given, double delta_given,\
		   double * average_intensity, double * points_used);


 public:

  //XXX
  int set_shell_resolution(float resolution) { _shell_resolution = resolution; return 1; }
  int set_hwhm(float hwhm_R, float hwhm_zs) { _hwhm_R = hwhm_R; _hwhm_zs = hwhm_zs; return 1; }
  


  diber_scores(diber_dataset* dataset) {input_diber_dataset = dataset; hkls_p1 = new clipper::HKL_info(); hkldata_esige_p1 = new clipper::HKL_data<clipper::datatypes::E_sigE<float> >; hkldata_esige_originalspg = new clipper::HKL_data<clipper::datatypes::E_sigE<float> >; _shell_resolution = 3.4; _hwhm_R = 0.09; _hwhm_zs = 0.02; };

  ~diber_scores() { delete(hkldata_esige_originalspg); delete(hkldata_esige_p1); delete(hkls_p1); };

  Diber_Error* calculate_all_averages();

  


  Diber_Error* run_phaser(char * hklin_fname, char * FP_label, string phaser_root);


};





//*******************************************************************************
//******************************* OUTPUT ****************************************
//*******************************************************************************


class diber_output
{

  
 public:

  
  diber_output( diber_dataset * input ) { diber_dataset_input_=input; };
  ~diber_output() {};
  
  Diber_Error* create_output(char * psfilename);
  std::string get_plain_english_output() { return plain_english_output_one_string_; };
    

 private:
  //---------------------     data     -----------------

  diber_dataset* diber_dataset_input_;
  Diber_Error* local_error_;
   
  //PostScript
  FILE * psfile_;
  float * rgb_;
  int maxcolor_;
  char * psfilename_;
  
  std::string plain_english_output_one_string_;


  //SVM models, defined in a files diber_model*.dat, were trained
  //with all input data scaled linearly from 0 to 1. Parameters 
  //defined below are used to put it back on a proper scale.
  static const float diber_model_input_data_xmax_=0.04;
  static const float diber_model_input_data_ymax_=10.0;

  static const float phaser_model_input_data_xmax_=0.04;
  static const float phaser_model_input_data_ymax_=600.0;
  static const float phaser_model_input_data_yshift_=200.0;



  //---------------------    tools     -----------------
  float bound( float min, float val, float max ) 
    { return ( (val < max) ? ( (val > min ) ? val : min ) : max ); }

  Diber_Error* play_with_svm(FILE * psfile);
  Diber_Error* play_with_svm_phaser_enhanced(FILE * psfile);

  vector<string>* prepare_plain_english_output( int predict_label, double * prob_estimates, const float improbable_cutoff);


  Diber_Error* open_new_postscript_file(char * psfilename, int legend);
  Diber_Error* close_postscript_file();

  //--------------- stdout peripherals -----------------

  int draw_horizontal_bar(float pbty);



  //------------------------------- POSTSCRIPT ---------------------------------
  //------------------ contour plot on a spherical projection ------------------
  //----------------------------------------------------------------------------



  class ps_projection{
  private:
    FILE * psfile__;
    float * rgb_;
    float ps_lowercornerx_, ps_lowercornery_;
    float ps_width_;
    int maxcolor_;
    int path_no_;
    float bound( float min, float val, float max ) 
      { return ( (val < max) ? ( (val > min ) ? val : min ) : max ); }
    int cartesian_to_spherical(double x, double y, double z,\
			       double * theta, double * delta, double * r);
    int redistribute_values(int data_size, vector<float>* xydata,\
			    vector<float>* xyfactors, float x, float y, float val);

    int contour_tracer(FILE * psfile, double *x_data, double *y_data, double *z_data, float data_size, float level);

  public:

    ps_projection(FILE * psfile, double ps_lowercornerx, double ps_lowercornery, double ps_width)\
                       {psfile__ = psfile;\
			ps_lowercornerx_ = ps_lowercornerx;\
			ps_lowercornery_ = ps_lowercornery;\
			ps_width_ = ps_width; rgb_=set_rgb(&maxcolor_);path_no_=100;};
    
    
    int draw_projection( diber_dataset  * input_diber_dataset, int n_contour_levels );
    int mark_phaser_solutions ( vector<float>* euler_angles, float size, float lwidth );
    
  };


  //------------------------------- POSTSCRIPT ---------------------------------
  //--------------- custom graph in cartesian coordiantes ----------------------
  //----------------------------------------------------------------------------



  class ps_graph{

  private:
    FILE * psfile__;
    float * rgb_;
    float local_xmin_, local_xmax_, local_ymin_, local_ymax_;
    float ps_lowercornerx_, ps_lowercornery_;
    float ps_xwidth_, ps_ywidth_; 
    int maxcolor_; 
    int path_no_;
    int use_diber_;


    float bound( float& min, float& val, float& max )\
      { return ( (val < max) ? ( (val > min ) ? val : min ) : max ); }

    inline bool contains( float& min, float& val, float& max  )\
      const { return ( val >= min && val <= max ); }


  public:
    ps_graph(FILE * psfile, float local_xmin, float local_xmax, float local_ymin,\
	  float local_ymax, float ps_lowercornerx, float ps_lowercornery, float ps_xwidth,\
	     float ps_ywidth, int use_diber){psfile__ = psfile;\
                        local_xmin_=local_xmin;\
			local_xmax_=local_xmax;\
			local_ymin_=local_ymin;\
			local_ymax_=local_ymax;\
			ps_lowercornerx_=ps_lowercornerx;\
			ps_lowercornery_=ps_lowercornery;\
			ps_xwidth_=ps_xwidth;\
			ps_ywidth_=ps_ywidth;rgb_=set_rgb(&maxcolor_); path_no_=0; use_diber_=use_diber;};
    
    ~ps_graph(){};
    int draw_line(float x1, float y1, float x2, float y2, float lwidth);
    int draw_frame(float lwidth, float tickstep_x, float tickstep_y);
    int draw_path(char * filename, float lwidth);
    int draw_filledpath(char * filename, float r, float g, float b);
    Diber_Error* draw_svm_classification(int predicted_class);
    int draw_marker(float x, float y, float size, float lwidth);
    int add_labels_dirty();
  };

  

};

