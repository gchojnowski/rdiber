#include <boost/python.hpp>
#include "../src/rdiber_funcs.h"
#include <string>
#include <boost/python/exception_translator.hpp>
#include <exception>



class rdiber_project{
 private:
  boost::python::list py_mtz_columns_;
  std::string mtz_filename_;
  diber_dataset * my_dataset_;
  diber_scores * my_scores_;
  diber_output * my_output_;
  Diber_Error * rdiberr_;
  
  std::string labin_f_;
  std::string labin_sigf_;
  
 public:
  rdiber_project();
  rdiber_project( std::string filename );
  rdiber_project( std::string filename, std::string labin_f, std::string labin_sigf, int use_diber );
  ~rdiber_project();

  boost::python::list get_columns() { return py_mtz_columns_; };

  int test_mtz_file( std::string filename );
  int calc_averages();
  int set_phaser_score(float phaser_llg);
  std::string prepare_output(std::string filename);
  int run_phaser( std::string labin_f );

  boost::python::dict get_svm_results();


  std::string get_filename();
  std::string get_colnames();
  std::string get_spgname();
  boost::python::list get_cellsize();
  double get_resmax();
  double get_resmin();
	

  // new from 24-02-2011
  boost::python::dict get_parameters();
  int set_shell_resolution(float resolution);
  int set_hwhm(float hwhm_R, float hwhm_zs);

  // new from 29-03-2011
  boost::python::list get_averages();




};

/*

boost::python::list get_columns(std::string filename){

  boost::python::list pycolumns;
  vector<string> words;
  vector<string> col_n_type;


  Diber_Error * diberr;
  diber_dataset * my_dataset;
  my_dataset = new diber_dataset();
  vector<clipper::String> mtz_columns;
  mtz_columns.resize(0, "");
  diberr = my_dataset->list_mtzfile_columns((char*)filename.c_str(), &mtz_columns);

  //file with clipper flags definitions: /xray/local_libs/clipper_2.1/clipper/clipper-2.1/clipper/ccp4/ccp4_mtz_io.cpp
  
  for(int i_col=0; i_col<mtz_columns.size(); i_col++){
    words = split_into_words(mtz_columns[i_col].c_str(), "/", false);
    col_n_type = split_into_words(words[words.size()-1].c_str(), " ", false);
    if(col_n_type[1].find('F')<string::npos || col_n_type[1].find('Q')<string::npos || col_n_type[1].find('J')<string::npos)
      pycolumns.append(boost::python::make_tuple(col_n_type[0], col_n_type[1]));
  }

  
  delete(my_dataset);
  return pycolumns;
}



int open_mtzfile(std::string filename){
  Diber_Error * diberr;
  diber_dataset * my_dataset;
  my_dataset = new diber_dataset();
  
  diberr = my_dataset->read_mtzfile((char*)filename.c_str(), (char*)"FP", (char*)"SIGFP");

  delete(my_dataset);

  return 1;
}
*/


