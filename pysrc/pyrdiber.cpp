#include "pyrdiber.h"

// file with clipper flags definitions: 
// ../clipper/clipper/ccp4/ccp4_mtz_io.cpp 




void translate(std::exception e)
{
    // Use the Python 'C' API to set up an exception object
    PyErr_SetString(PyExc_RuntimeError, "Clipper fatal...");
}




int rdiber_project::test_mtz_file( std::string mtz_filename ){
  CMtz::MTZ* mtzin = CMtz::MtzGet( (char*)mtz_filename.c_str(), 0 );
  if(mtzin == NULL)
    return 0;
  else
    return 1;
}




rdiber_project::rdiber_project(){

  my_dataset_ = new diber_dataset();
  my_scores_ = new diber_scores( my_dataset_ );
  my_output_ = new diber_output( my_dataset_ );

}



rdiber_project::rdiber_project(std::string mtz_filename){

  vector<string> words;
  vector<string> col_n_type;
  vector<clipper::String> mtz_columns;

  
  my_dataset_ = new diber_dataset();
  my_scores_ = new diber_scores( my_dataset_ );
  my_output_ = new diber_output( my_dataset_ );
  mtz_filename_ = mtz_filename;


  
  mtz_columns.resize(0, "");


  rdiberr_ = my_dataset_->list_mtzfile_columns((char*)mtz_filename_.c_str(), &mtz_columns);
  

  if( !rdiberr_->Number()){
  
    for(int i_col=0; i_col<mtz_columns.size(); i_col++){
      words = split_into_words(mtz_columns[i_col].c_str(), "/", false);
      col_n_type = split_into_words(words[words.size()-1].c_str(), " ", false);
      if( col_n_type[1].find('F')<string::npos		\
	  || col_n_type[1].find('Q')<string::npos || col_n_type[1].find('J')<string::npos )
	py_mtz_columns_.append(boost::python::make_tuple(col_n_type[0], col_n_type[1]));
    }
    
    mtz_columns.clear();
  }
}




rdiber_project::rdiber_project(std::string mtz_filename, std::string labin_f, std::string labin_sigf, int use_diber){
  vector<string> words;
  vector<string> col_n_type;
  string ctruncate_command;
  vector<clipper::String> mtz_columns;


  my_dataset_ = new diber_dataset( use_diber );
  my_scores_ = new diber_scores( my_dataset_ );
  my_output_ = new diber_output( my_dataset_ );
  mtz_filename_ = mtz_filename;

  labin_f_ = labin_f;
  labin_sigf_ = labin_sigf;

  mtz_columns.resize(0, "");
  rdiberr_ = my_dataset_->list_mtzfile_columns((char*)mtz_filename_.c_str(), &mtz_columns);
  for(int i_col=0; i_col<mtz_columns.size(); i_col++){
    words = split_into_words(mtz_columns[i_col].c_str(), "/", false);
    col_n_type = split_into_words(words[words.size()-1].c_str(), " ", false);
    if( col_n_type[0].find(labin_f)<string::npos && col_n_type[1].find("J")<string::npos ){
      cout<<"\n\n *************************************************************************"<<endl;
      cout<<" **      WARNING: dataset contains intensities, running ctruncate.      **"<<endl;
      cout<<" *************************************************************************"<<endl<<endl;
      ctruncate_command.clear();
      ctruncate_command = "${CBIN}/ctruncate -mtzin "+mtz_filename\
	+" -mtzout " + mtz_filename				\
	+" -colin '*/*/["+labin_f+","+labin_sigf+"]' -colout DIBER";
      
      cout<<ctruncate_command.c_str()<<endl;

      system(ctruncate_command.c_str());

      labin_f_ = "F_DIBER";
      labin_sigf_ = "SIGF_DIBER";
    }
  }

 
  rdiberr_ = my_dataset_->read_mtzfile( (char*)mtz_filename_.c_str(),\
				       (char*)labin_f_.c_str(),\
				       (char*)labin_sigf_.c_str() );
  
}



rdiber_project::~rdiber_project(){
  delete(my_output_);
  delete(my_scores_);
  delete(my_dataset_);
}



int rdiber_project::calc_averages(){
  
  rdiberr_ = my_scores_->calculate_all_averages();
  cout<<rdiberr_->Message()<<endl;

  return 1;
}

int rdiber_project::set_phaser_score(float phaser_llg){
  
  my_dataset_->set_phaser_max_llg(phaser_llg);

  return 1;
}





std::string rdiber_project::prepare_output(std::string filename){

  rdiberr_ = my_output_->create_output( (char*)(filename+string(".output.ps")).c_str() );
  std::string plain_english_output = my_output_->get_plain_english_output();
  cout<<rdiberr_->Message()<<endl;

  return plain_english_output;
}



int rdiber_project::run_phaser( std::string labin_f ){
  
  rdiberr_ = my_scores_->run_phaser( (char*)mtz_filename_.c_str(), (char*)labin_f_.c_str(), \
       				    (char*)(mtz_filename_ + string(".phaser")).c_str() );


  return 1;
}


//---------------------------
//simple tools

boost::python::dict rdiber_project::get_svm_results(){
  boost::python::dict pbty_estimates;
  pbty_estimates["complex"] = my_dataset_->get_svm_prob_estimates()[0];
  pbty_estimates["dna"] = my_dataset_->get_svm_prob_estimates()[1];
  pbty_estimates["protein"] =  my_dataset_->get_svm_prob_estimates()[2];

  return pbty_estimates;

    
}


std::string rdiber_project::get_filename(){
  return std::string(my_dataset_->get_hklin_fname());
}

std::string rdiber_project::get_colnames(){
  return std::string(my_dataset_->get_labin());
}

std::string rdiber_project::get_spgname(){
  return my_dataset_->get_hkls().spacegroup().symbol_hm();
}

boost::python::list rdiber_project::get_cellsize(){
  
  boost::python::list uc_size;
  
  uc_size.append(my_dataset_->get_hkls().cell().a());
  uc_size.append(my_dataset_->get_hkls().cell().b());
  uc_size.append(my_dataset_->get_hkls().cell().c());
  uc_size.append(my_dataset_->get_hkls().cell().alpha_deg());
  uc_size.append(my_dataset_->get_hkls().cell().beta_deg());
  uc_size.append(my_dataset_->get_hkls().cell().gamma_deg());
  
  return uc_size;
}

double rdiber_project::get_resmax(){
  if(my_dataset_->get_hkldata_fsigf().invresolsq_range().max()>0.0)
  	return sqrt(1.0/my_dataset_->get_hkldata_fsigf().invresolsq_range().max());
  else
	return -3.0;
}

double rdiber_project::get_resmin(){
  if(my_dataset_->get_hkldata_fsigf().invresolsq_range().min()>0.0)
  	return sqrt(1.0/my_dataset_->get_hkldata_fsigf().invresolsq_range().min());
  else
	return -3.0;
}


//----------------------------
//----------averages--------


int rdiber_project::set_shell_resolution(float resolution){
  my_scores_->set_shell_resolution(resolution);
  return 1;
}

int rdiber_project::set_hwhm(float hwhm_R, float hwhm_zs){
  my_scores_->set_hwhm(hwhm_R, hwhm_zs);
  return 1;
}



boost::python::dict rdiber_project::get_parameters(){
  boost::python::dict rdiber_parameters;
  rdiber_parameters["cellsize"] = my_dataset_->get_oneovercrVol();
  rdiber_parameters["highest_average"] = my_dataset_->get_max_average();
  rdiber_parameters["highest_average_L"] =  my_dataset_->get_max_average_L();
  rdiber_parameters["phaser_score"] =  my_dataset_->get_phaser_max_llg();

  return rdiber_parameters;


}

// ------------------------------------------------------------------------------------------
// returns a list of all average intensities and corresponding number of averaged RS points...

boost::python::list rdiber_project::get_averages(){
  boost::python::list rdiber_averages;
  
  for(int iq=0; iq<my_dataset_->get_icodata().size(); iq++){
	rdiber_averages.append(my_dataset_->get_icodata()[iq].delta);
	rdiber_averages.append(my_dataset_->get_icodata()[iq].theta);		
	rdiber_averages.append(my_dataset_->get_icodata()[iq].average_intensity);
  }

  return rdiber_averages;


}

// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------





BOOST_PYTHON_MODULE(pyrdiber)
{
  using namespace boost::python;


  register_exception_translator<std::exception>(translate);


  class_<rdiber_project> dp( "rdiber_project", init<std::string, std::string, std::string, int>() );
  dp.def(init<std::string>());
  dp.def(init<>());

  dp.def("test_mtz_file", &rdiber_project::test_mtz_file);
  dp.def("get_columns", &rdiber_project::get_columns);
  dp.def("calc_averages", &rdiber_project::calc_averages);
  dp.def("set_phaser_score", &rdiber_project::set_phaser_score);
  dp.def("prepare_output", &rdiber_project::prepare_output);
  dp.def("run_phaser", &rdiber_project::run_phaser);


  dp.def("get_svm_results", &rdiber_project::get_svm_results);
  dp.def("get_filename", &rdiber_project::get_filename);
  dp.def("get_colnames", &rdiber_project::get_colnames);
  dp.def("get_spgname", &rdiber_project::get_spgname);
  dp.def("get_cellsize", &rdiber_project::get_cellsize);
  dp.def("get_resmax", &rdiber_project::get_resmax);
  dp.def("get_resmin", &rdiber_project::get_resmin);

  dp.def("get_parameters", &rdiber_project::get_parameters);
  dp.def("set_shell_resolution", &rdiber_project::set_shell_resolution);
  dp.def("set_hwhm", &rdiber_project::set_hwhm);


  dp.def("get_averages",  &rdiber_project::get_averages);


}
