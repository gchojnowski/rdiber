# DIBER: protein, DNA or both?

RIBER/DIBER judges nucleic acid content of a crystal based solely on diffraction data, before the structure is solved.
A web server and OSX binaries are available at http://diber.iimcb.gov.pl/

If you find this tool useful please cite one of the related publications

- Grzegorz Chojnowski and Matthias Bochtler Acta Cryst. (2010), D66, 643-653 [pdf](https://scripts.iucr.org/cgi-bin/paper?dz5189)
- Grzegorz Chojnowski, Janusz M. Bujnicki, and Matthias Bochtler Bioinformatics. (2012), 28(6), 880–881 [pdf](https://academic.oup.com/bioinformatics/article/28/6/880/310616)

# Installation

to compile diber type

```
cd /path/to/diber/src
make
```

the program requires a system a system variable RDIBER pointing to the installation directory. you can add the followint line to your `.bashrc` file

```
export RDIBER=/path/to/diber
export PATH=$PATH:/path/to/diber/bin
```

finally, you can test your installation with one of datasets from example directory

```
cd /path/to/diber/examples
rdiber -mtzin dna.mtz -f F -sigf SIGF -mode diber
```

More detailed installation are in the README file 


(C) 2010-2020 Grzegorz Chojnowski