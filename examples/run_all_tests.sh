#! /bin/bash

export RDIBER=`pwd`/../

${RDIBER}/bin/rdiber -mtzin dna.mtz -f F -sigf SIGF -psout dna.ps -mode diber -phaser
${RDIBER}/bin/rdiber -mtzin protein.mtz -f FP -sigf SIGFP -psout protein.ps -mode riber
${RDIBER}/bin/rdiber -mtzin protein_dna.mtz -f FP -phaser -sigf SIGFP -psout protein_dna.ps -mode diber
${RDIBER}/bin/rdiber -mtzin rna.mtz -f FP -sigf SIGFP -psout rna.ps -mode riber
