// Clipper app to perform inverse ffts
/* Copyright 2003-2004 Kevin Cowtan & University of York all rights reserved */

#include <clipper/clipper.h>
#include <clipper/clipper-contrib.h>
#include <clipper/clipper-ccp4.h>
#include <clipper/clipper-mmdb.h>

extern "C" {
#include <stdlib.h>
}


int main( int argc, char** argv )
{
  CCP4Program prog( "csfcreate.cpp", "0.1", "$Date: 2004/06/01" );

  // defaults
  clipper::String title;
  clipper::String ipmap = "NONE";
  clipper::String ippdb = "NONE";
  clipper::String opfile = "sfcreate.mtz";
  clipper::String opcol = "sfcreate";
  clipper::Resolution reso( 2.0 );

  // command input
  CCP4CommandInput args( argc, argv, true );
  int arg = 0;
  while ( ++arg < args.size() ) {
    if ( args[arg] == "-title" ) {
      if ( ++arg < args.size() ) title = args[arg];
    } else if ( args[arg] == "-mapin" ) {
      if ( ++arg < args.size() ) ipmap = args[arg];
    } else if ( args[arg] == "-pdbin" ) {
      if ( ++arg < args.size() ) ippdb = args[arg];
    } else if ( args[arg] == "-mtzout" ) {
      if ( ++arg < args.size() ) opfile = args[arg];
    } else if ( args[arg] == "-colout" ) {
      if ( ++arg < args.size() ) opcol = args[arg];
    } else if ( args[arg] == "-resolution" ) {
      if ( ++arg < args.size() ) {
	reso = clipper::Resolution( clipper::String(args[arg]).f() );
      }
    } else {
      std::cout << "Unrecognized:\t" << args[arg] << "\n";
      args.clear();
    }
  }
  if ( args.size() <= 1 ) {
    std::cout << "Usage: csfcreate.cpp\n\t-mapin <filename>\n\t-pdbin <filename>\n\t-resolution <resolution/A>\n\t-mtzout <filename>\n\t-colout <colpath>\nCreate structure factor file from map or pdb.\n";
    exit(1);
  }

  // get crystal and dataset names
  clipper::String colnm = "";
  clipper::String prjnm = "project";
  clipper::String xtlnm = "xtal";
  clipper::String setnm = "dset";
  std::vector<clipper::String> oppath = opcol.split("/");
  colnm = oppath[oppath.size()-1];
  if ( oppath.size() >= 2 ) setnm = oppath[oppath.size()-2];
  if ( oppath.size() >= 3 ) xtlnm = oppath[oppath.size()-3];

  // make data objects
  clipper::CCP4MTZfile mtzout;
  clipper::Spacegroup spgr;
  clipper::Cell cell;
  clipper::HKL_info hkls;
  clipper::HKL_data< clipper::data32::F_phi> fphi( hkls );

  if ( ipmap != "NONE" ) {

    // import xmap
    clipper::Xmap<float> xmap;
    clipper::CCP4MAPfile mapin;
    mapin.open_read( ipmap );
    mapin.import_xmap( xmap );
    mapin.close_read();
    // calc structure factors
    hkls = clipper::HKL_info( xmap.spacegroup(), xmap.cell(), reso, true );
    fphi = clipper::HKL_data< clipper::data32::F_phi>( hkls );
    xmap.fft_to( fphi );

  } else if ( ippdb != "NONE" ) {

    // import pdb
    clipper::MMDBManager mmdb;
    const int mmdbflags = MMDBF_IgnoreBlankLines | MMDBF_IgnoreDuplSeqNum | MMDBF_IgnoreNonCoorPDBErrors | MMDBF_IgnoreRemarks;
    mmdb.SetFlag( mmdbflags );
    mmdb.ReadPDBASCII( (char*)ippdb.c_str() );
    // get a list of all the atoms
    clipper::mmdb::PPCAtom psel;
    int hndl, nsel;
    hndl = mmdb.NewSelection();
    mmdb.SelectAtoms( hndl, 0, 0, SKEY_NEW );
    mmdb.GetSelIndex( hndl, psel, nsel );
    clipper::MMDBAtom_list atoms( psel, nsel );
    mmdb.DeleteSelection( hndl );
    // calc structure factors
    hkls = clipper::HKL_info( mmdb.spacegroup(), mmdb.cell(), reso, true );
    fphi = clipper::HKL_data< clipper::data32::F_phi>( hkls );
    clipper::SFcalc_aniso_fft<float> sfc;
    sfc( fphi, atoms );

  } else {
    std::cerr << "ERROR: Neither map nor pdb given" << std::endl;
  }

  // output data
  clipper::MTZcrystal mxtl( xtlnm, prjnm, hkls.cell());
  clipper::MTZdataset mset( setnm, 1.0 );
  mtzout.open_write( opfile );
  mtzout.export_hkl_info( hkls );
  mtzout.export_crystal( mxtl, "/"+xtlnm );
  mtzout.export_dataset( mset, "/"+xtlnm+"/"+setnm );
  mtzout.export_hkl_data( fphi, "/"+xtlnm+"/"+setnm+"/"+colnm );
  mtzout.close_write();
}
